const mongoose = require("mongoose");
const { Schema } = mongoose;

const wayforpaySchema = new Schema({
    account: String,
    private: String,
});

module.exports = mongoose.model("wayforpay", wayforpaySchema);
