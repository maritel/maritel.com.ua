const mongoose = require("mongoose");
const { Schema } = mongoose;

const requestsFromIpSchema = new Schema({
    ip: String,
    date: { type: Date, default: Date.now },
});

module.exports = mongoose.model("requestsFromIp", requestsFromIpSchema);
