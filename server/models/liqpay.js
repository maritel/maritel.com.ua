const mongoose = require("mongoose");
const { Schema } = mongoose;

const liqpaySchema = new Schema({
    public: String,
    private: String,
});

module.exports = mongoose.model("liqpay", liqpaySchema);
