const mongoose = require("mongoose");
const { Schema } = mongoose;

const moyskladProducts = new Schema({
    moyskladId: { type: String, unique: true },
    requestId: String,
    article: String,
});

module.exports = mongoose.model("moyskladProducts", moyskladProducts);
