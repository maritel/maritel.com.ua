const mongoose = require("mongoose");
const { Schema } = mongoose;

const HomePageBannersSchema = new Schema({
    desktop: String,
    mobile: String,
});

module.exports = mongoose.model("homePageBanners", HomePageBannersSchema);
