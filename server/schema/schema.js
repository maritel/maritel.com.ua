const graphql = require("graphql");

const mongoose = require("mongoose");

const {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull,
    GraphQLString,
    GraphQLID,
    GraphQLInt,
} = graphql;

const { createMoyskladOrderSubscriber } = require("../moysklad/order");

const Product = require("../models/product");

const Category = require("../models/category");
const SpecCategs = require("../models/specCategs");
const Subscr = require("../models/subscrByMail");
const Colors = require("../models/colors");
const Customer = require("../models/customer");
const ProductSubscriber = require("../models/productSubscriber");
const Order = require("../models/order");
const MainSettings = require("../models/mainSettings");
const Promo = require("../models/promo");
const Banner = require("../models/banners");
const HomePageBanner = require("../models/homePageBanners");

const ProductType = new GraphQLObjectType({
    name: "Product",
    fields: () => ({
        id: { type: GraphQLID },
        uuid: { type: GraphQLString },
        title: { type: GraphQLString },
        descr: { type: GraphQLString },
        color: { type: GraphQLString },
        price: { type: GraphQLString },
        gender: { type: GraphQLString },
        modelParam: { type: GraphQLString },
        care: { type: GraphQLString },
        composition: { type: GraphQLString },
        sizes: { type: GraphQLString },
        lastPrice: { type: GraphQLString },
        type: { type: GraphQLString },
        photos: { type: new GraphQLList(GraphQLString) },
        previewPhoto: { type: GraphQLString },
        timestamp: { type: GraphQLString },
    }),
});

const SpecCategType = new GraphQLObjectType({
    name: "SpecCateg",
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        products: { type: new GraphQLList(GraphQLString) },
    }),
});

const ColorsType = new GraphQLObjectType({
    name: "Colors",
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        link: { type: GraphQLString },
    }),
});

const OptionType = new GraphQLObjectType({
    name: "OptionType",
    fields: {
        value: { type: GraphQLString },
        name: { type: GraphQLString },
    },
});

const ShippingAddressType = new GraphQLObjectType({
    name: "CustomerShippingAddress",
    fields: {
        street: {
            type: OptionType,
        },
        houseNumber: { type: GraphQLString },
        appartment: { type: GraphQLString },
        value: { type: GraphQLString },
        name: { type: GraphQLString },
    },
});

const CategoriesType = new GraphQLObjectType({
    name: "Categories",
    fields: () => ({
        _id: { type: GraphQLID },
        category: { type: GraphQLString },
        subCategories: { type: GraphQLString },
        banners: { type: new GraphQLList(BannerType) },
    }),
});

const CartItemsType = new GraphQLObjectType({
    name: "CartItems",
    fields: () => ({
        prodUuid: { type: GraphQLString },
        name: { type: GraphQLString },
        size: { type: GraphQLString },
        quantity: { type: GraphQLString },
        price: { type: GraphQLString },
    }),
});

const OrderType = new GraphQLObjectType({
    name: "Orders",
    fields: () => ({
        _id: { type: GraphQLID },
        orderId: { type: GraphQLString },
        items: { type: new GraphQLList(CartItemsType) },
        city: { type: OptionType },
        customReceiver: { type: graphql.GraphQLBoolean },
        paymentMethod: { type: GraphQLString },
        paymentService: { type: GraphQLString },
        shippingAddress: { type: ShippingAddressType },
        shippingMethod: { type: GraphQLString },
        amount: { type: GraphQLString },
        paymentStatus: { type: GraphQLString },
        customer: { type: GraphQLString },
        date: { type: GraphQLString },
    }),
});

const CustomerType = new GraphQLObjectType({
    name: "Customer",
    fields: {
        _id: { type: GraphQLString },
        email: { type: GraphQLString },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        phone: { type: GraphQLString },
        city: { type: OptionType },
        birthday: {
            type: new GraphQLObjectType({
                name: "CustomerBirthday",
                fields: {
                    day: { type: GraphQLString },
                    month: { type: GraphQLString },
                },
            }),
        },
        shippingMethod: {
            type: new GraphQLObjectType({
                name: "CustomerShippingMethod",
                fields: {
                    value: { type: GraphQLString },
                    name: { type: GraphQLString },
                },
            }),
        },
        shippingAddress: { type: ShippingAddressType },
        orders: { type: new GraphQLList(OrderType) },
        gender: {
            type: new GraphQLObjectType({
                name: "CustomerGender",
                fields: {
                    value: { type: GraphQLString },
                    name: { type: GraphQLString },
                },
            }),
        },
        status: { type: GraphQLString },
        bonuses: { type: GraphQLInt },
    },
});

const SubscribeType = new GraphQLObjectType({
    name: "SubscribeByMail",
    fields: () => ({
        id: { type: GraphQLID },
        email: { type: GraphQLString },
    }),
});

const MainSettingsType = new GraphQLObjectType({
    name: "MainSettings",
    fields: () => ({
        email: { type: GraphQLString },
        phone: { type: GraphQLString },
        instagram: { type: GraphQLString },
        facebook: { type: GraphQLString },
        telegram: { type: GraphQLString },
    }),
});

const PromoType = new GraphQLObjectType({
    name: "Promo",
    fields: {
        promoName: { type: GraphQLString },
        promoDisc: { type: GraphQLString },
        promoValue: { type: GraphQLInt },
    },
});

const BannerType = new GraphQLObjectType({
    name: "Banner",
    fields: {
        _id: { type: GraphQLID },
        category: { type: GraphQLID },
        position: { type: GraphQLString },
        accentedText: { type: GraphQLString },
        text: { type: GraphQLString },
        title: { type: GraphQLString },
        buttonText: { type: GraphQLString },
        link: { type: GraphQLString },
        image: { type: GraphQLString },
        for: { type: GraphQLString },
    },
});

const BannerCarouselType = new GraphQLObjectType({
    name: "BannerCarousel",
    fields: {
        title: { type: GraphQLString },
    },
});

const BannerSpecialType = new GraphQLObjectType({
    name: "BannerSpecial",
    fields: {
        accentedText: { type: GraphQLString },
        text: { type: GraphQLString },
        buttonText: { type: GraphQLString },
        link: { type: GraphQLString },
    },
});

const HomePageBannerType = new GraphQLObjectType({
    name: "HomePageBanner",
    fields: {
        desktop: { type: GraphQLString },
        mobile: { type: GraphQLString },
    },
});

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        addSubscribe: {
            type: SubscribeType,
            args: {
                email: { type: new GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, { email }) {
                const sub = new Subscr({
                    email,
                });

                sub.save();
            },
        },
        EditCustomerInfo: {
            type: CustomerType,
            args: {
                _id: { type: GraphQLString },
                customer: {
                    type: new graphql.GraphQLInputObjectType({
                        name: "CustomerInput",
                        fields: {
                            email: { type: GraphQLString },
                            firstName: { type: GraphQLString },
                            lastName: { type: GraphQLString },
                            phone: { type: GraphQLString },
                            city: {
                                type: new graphql.GraphQLInputObjectType({
                                    name: "CustomerInputCity",
                                    fields: {
                                        value: { type: GraphQLString },
                                        name: { type: GraphQLString },
                                    },
                                }),
                            },
                            birthday: {
                                type: new graphql.GraphQLInputObjectType({
                                    name: "CustomerInputBirthday",
                                    fields: {
                                        day: { type: GraphQLString },
                                        month: { type: GraphQLString },
                                    },
                                }),
                            },
                            shippingMethod: {
                                type: new graphql.GraphQLInputObjectType({
                                    name: "CustomerInputShippingMethod",
                                    fields: {
                                        value: { type: GraphQLString },
                                        name: { type: GraphQLString },
                                    },
                                }),
                            },
                            shippingAddress: {
                                type: new graphql.GraphQLInputObjectType({
                                    name: "CustomerInputShippingAddress",
                                    fields: {
                                        street: {
                                            type: new graphql.GraphQLInputObjectType(
                                                {
                                                    name: "CustomerInputStreet",
                                                    fields: {
                                                        value: {
                                                            type: GraphQLString,
                                                        },
                                                        name: {
                                                            type: GraphQLString,
                                                        },
                                                    },
                                                }
                                            ),
                                        },
                                        houseNumber: { type: GraphQLString },
                                        appartment: { type: GraphQLString },
                                        value: { type: GraphQLString },
                                        name: { type: GraphQLString },
                                    },
                                }),
                            },
                            gender: {
                                type: new graphql.GraphQLInputObjectType({
                                    name: "CustomerInputGender",
                                    fields: {
                                        value: { type: GraphQLString },
                                        name: { type: GraphQLString },
                                    },
                                }),
                            },
                            status: { type: GraphQLString },
                        },
                    }),
                },
            },
            async resolve(_parent, args, context) {
                if (!context.customer) {
                    return {};
                }

                try {
                    await Customer.updateOne(
                        { _id: context.customer.customerId },
                        { ...args.customer }
                    );

                    return Customer.findOne({
                        _id: context.customer.customerId,
                    });
                } catch {
                    return {};
                }
            },
        },
        AddProductSubscriber: {
            type: graphql.GraphQLBoolean,
            args: {
                name: { type: GraphQLString },
                email: { type: GraphQLString },
                phone: { type: GraphQLString },
                product: { type: GraphQLString },
                size: { type: GraphQLString },
                subscribe: { type: graphql.GraphQLBoolean },
            },
            async resolve(_parent, args) {
                try {
                    if (args.subscribe) {
                        await Subscr.create({ email: args.email });
                    }
                } catch {}

                try {
                    const product = await Product.findOne({
                        uuid: args.product,
                    });

                    if (!product) {
                        return false;
                    }

                    await ProductSubscriber.create({
                        ...args,
                        product: product._id,
                    });

                    createMoyskladOrderSubscriber({ ...args, product });

                    return true;
                } catch {
                    return false;
                }
            },
        },
    },
});

const Query = new GraphQLObjectType({
    name: "Query",
    fields: {
        specialBanner: {
            type: BannerSpecialType,
            async resolve() {
                return Banner.findOne({ for: "special" });
            },
        },
        mainSettings: {
            type: MainSettingsType,
            async resolve() {
                return MainSettings.findOne({});
            },
        },
        categories: {
            type: new GraphQLList(CategoriesType),
            async resolve() {
                return Category.aggregate([
                    { $match: {} },
                    {
                        $lookup: {
                            from: "banners",
                            localField: "_id",
                            foreignField: "category",
                            as: "banners",
                        },
                    },
                ]);
            },
        },
        getSpecCateg: {
            type: new GraphQLList(SpecCategType),
            resolve() {
                return SpecCategs.find({});
            },
        },
        product: {
            type: ProductType,
            args: { uuid: { type: GraphQLString } },
            resolve(_parent, args) {
                return Product.findOne({ uuid: args.uuid });
            },
        },
        products: {
            type: new GraphQLList(ProductType),
            resolve() {
                return Product.find({});
            },
        },

        productsByCategory: {
            type: new GraphQLList(ProductType),
            args: { type: { type: GraphQLString } },
            resolve(_parent, { type }) {
                return Product.find({ type: new RegExp(type + ".*", "g") });
            },
        },

        productsFromSpecialCategory: {
            type: new GraphQLObjectType({
                name: "SpecialCategoryWithProducts",
                fields: {
                    name: { type: GraphQLString },
                    products: { type: new GraphQLList(ProductType) },
                },
            }),
            args: { name: { type: GraphQLString } },
            async resolve(_, { name }) {
                const specialCategories = await SpecCategs.find({});

                const specialCategory = specialCategories.find(
                    (category) =>
                        category.name.toLowerCase() === name.toLowerCase()
                );

                if (specialCategory) {
                    const products = await Product.find({
                        _id: specialCategory.products.map((id) =>
                            mongoose.Types.ObjectId(id)
                        ),
                    });
                    return { name: specialCategory.name, products };
                }

                return null;
            },
        },

        productsByName: {
            type: new GraphQLList(ProductType),
            args: { title: { type: GraphQLString } },
            resolve(_parent, { title }) {
                return Product.find({ title });
            },
        },

        productsFromCart: {
            type: new GraphQLList(ProductType),
            args: { uuid: { type: new GraphQLList(GraphQLString) } },
            resolve(_parent, { uuid }) {
                return Product.find({ uuid: new RegExp(uuid.join("|"), "g") });
            },
        },

        getRelatedProds: {
            type: new GraphQLList(ProductType),
            args: { title: { type: GraphQLString } },
            resolve(_parent, { title }) {
                return Product.find({ title });
            },
        },

        queryLimitProducts: {
            type: new GraphQLList(ProductType),
            args: { categ: { type: GraphQLString } },

            resolve(_, { categ }) {
                return Product.find({}).limit(50);
            },
        },

        queryProducts: {
            type: new GraphQLList(ProductType),
            args: {
                query: { type: GraphQLString },
            },
            async resolve(_p, { query }) {
                return Product.find({
                    title: new RegExp(query, "gi"),
                });
            },
        },
        colors: {
            type: new GraphQLList(ColorsType),
            resolve() {
                return Colors.find({});
            },
        },
        customer: {
            type: CustomerType,
            resolve(_parent, _args, context) {
                if (!context.customer) {
                    return {};
                }

                return Customer.findOne({
                    _id: context.customer.customerId,
                }).populate("orders");
            },
        },
        orders: {
            type: new GraphQLObjectType({
                name: "OrdersReturn",
                fields: {
                    orders: { type: new GraphQLList(OrderType) },
                    orderCount: { type: GraphQLString },
                },
            }),
            args: {
                limit: { type: GraphQLString },
                offset: { type: GraphQLString },
                sort: { type: GraphQLString },
            },
            async resolve(_parent, args, context) {
                if (!context.customer) {
                    return {};
                }

                const orders = await Order.find({
                    customer: context.customer.customerId,
                })
                    .sort(args.sort || "-date")
                    .skip(parseInt(args.offset) || 0)
                    .limit(parseInt(args.limit) || 5);

                const orderCount = Order.countDocuments({
                    customer: context.customer.customerId,
                });

                return { orders, orderCount };
            },
        },
        promo: {
            type: PromoType,
            args: {
                promoName: { type: GraphQLString },
            },
            resolve(_parent, args) {
                return Promo.findOne({ promoName: args.promoName });
            },
        },
        promos: {
            type: new GraphQLList(PromoType),
            resolve() {
                return Promo.find({});
            },
        },
        carousel: {
            type: new GraphQLList(BannerCarouselType),
            resolve() {
                return Banner.find({ for: "carousel" });
            },
        },
        homePageBanner: {
            type: HomePageBannerType,
            async resolve() {
                return await HomePageBanner.findOne({});
            },
        },
    },
});

module.exports = new GraphQLSchema({
    mutation: Mutation,
    query: Query,
});
