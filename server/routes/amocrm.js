const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

var https = require("https");

router.get("/amocrm-login", async (_, res) => {
    let cookieForAmoCrm = [];

    const login = () =>
        new Promise((resolve, reject) => {
            const body = JSON.stringify({
                USER_LOGIN: "goloborodko.marita@gmail.com",
                USER_HASH: "445680bedadc681774a7f038049e90f69663fb60",
            });

            const options = {
                host: "maritel.amocrm.ru",
                path: "/private/api/auth.php?type=json",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const request = https.request(options, (response) => {
                response.on("data", function (chunk) {
                    const responseObj = JSON.parse(chunk);

                    if (response.statusCode == 200) {
                        cookieForAmoCrm = response.headers["set-cookie"];
                        responseObj.coockie = cookieForAmoCrm;
                        return resolve(responseObj);
                    } else {
                        return reject(responseObj);
                    }
                });
            });

            request.write(body);
            request.end();
        });

    const result = await login();
    result.coockie = cookieForAmoCrm;
    res.json(result);
});

router.post("/create-lead", bodyParser.text(), (req, res) => {
    const { coockie, orderInfo, orderProductsInfo, orderTotal } = JSON.parse(
        req.body
    );
    const createLead = () =>
        new Promise((resolve, reject) => {
            const body = JSON.stringify({
                add: [
                    {
                        name: `${orderInfo.payer.firstName} ${orderInfo.payer.lastName}`,
                        price: orderTotal,
                        pipeline_id: 2220055,
                        custom_fields_values: [
                            {
                                field_id: 652435,
                                values: [
                                    {
                                        value: `${orderInfo.payer.firstName} ${orderInfo.payer.lastName}`,
                                    },
                                ],
                            },
                            {
                                field_id: 652453,
                                values: [
                                    {
                                        value: `${orderInfo.payer.phone}`,
                                    },
                                ],
                            },
                            {
                                field_id: 652443,
                                values: [
                                    {
                                        value: `${orderInfo.city.name}`,
                                    },
                                ],
                            },
                            {
                                field_id: 652437,
                                values: [
                                    {
                                        value: `${
                                            Object.keys(
                                                orderInfo.deliveryAddress
                                            ).length >= 1
                                                ? `${
                                                      orderInfo.deliveryAddress
                                                          .name ||
                                                      `${
                                                          orderInfo
                                                              .deliveryAddress
                                                              .street.name
                                                      } ${
                                                          orderInfo
                                                              .deliveryAddress
                                                              .houseNumber
                                                      }${
                                                          orderInfo
                                                              .deliveryAddress
                                                              .appartment
                                                              ? `/${orderInfo.deliveryAddress.appartment}`
                                                              : ""
                                                      }`
                                                  }`
                                                : ""
                                        }`,
                                    },
                                ],
                            },
                            {
                                field_id: 667458,
                                values: [
                                    {
                                        value: `${
                                            orderInfo.paymentMethod === "cash"
                                                ? "Наложенный платеж"
                                                : orderInfo.paymentMethod ===
                                                  "card"
                                                ? "Оплата картой"
                                                : ""
                                        }`,
                                    },
                                ],
                            },
                            {
                                field_id: 667460,
                                values: [
                                    {
                                        value: `${orderInfo.paymentService}`,
                                    },
                                ],
                            },
                            {
                                field_id: 667462,
                                values: [
                                    {
                                        value: `${
                                            orderInfo.customReceiver
                                                ? `${orderInfo.customReceiver.firstName} ${orderInfo.customReceiver.lastName} ${orderInfo.customReceiver.patronymic}`
                                                : ""
                                        }`,
                                    },
                                ],
                            },
                            {
                                field_id: 667464,
                                values: [
                                    {
                                        value: `${
                                            orderInfo.customReceiver
                                                ? `${orderInfo.customReceiver.phone}`
                                                : ""
                                        }`,
                                    },
                                ],
                            },
                            {
                                field_id: 667468,
                                values: [
                                    {
                                        value: `${orderProductsInfo.map(
                                            (item) =>
                                                `${item.name} (${item.size}) - ${item.price} грн (${item.quantity} шт); `
                                        )}`,
                                    },
                                ],
                            },
                        ],
                    },
                ],
            });

            const options = {
                host: "maritel.amocrm.ru",
                path: "/api/v4/leads",
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Cookie: coockie,
                },
            };

            const request = https.request(options, (response) => {
                response.on("data", (chunk) => {
                    if (response.statusCode == 200) {
                        return resolve(JSON.parse(chunk));
                    } else {
                        return reject(JSON.parse(chunk));
                    }
                });
            });
            request.write(body);
            request.end();
        });

    createLead().then(
        (result) => {
            console.log("success");
        },
        () => {
            console.log("error");
        }
    );
});

module.exports = router