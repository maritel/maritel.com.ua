const express = require("express");
const router = express.Router();
const crypto = require("crypto");
const Order = require("../models/order");
const Customer = require("../models/customer");
const Promo = require("../models/promo");
const Liqpay = require("../models/liqpay");

const { orderSchema } = require("../helpers/validationSchemas");
const calculateOrderTotal = require("../helpers/calculateOrderTotal");
const LiqPay = require("../helpers/liqpay.sdk");
const Wayforpay = require("../models/wayforpay");

const { createMoyskladOrder } = require("../moysklad/order");

const {
    checkLiqpayValidity,
    checkWayforpayValidity,
} = require("../helpers/checkOrderValidity");

const { checkPartPaymentValidity } = require("../helpers/liqpayPartPayment");

const { addRequestHistory } = require("../helpers/addRequestHistory");
const { recaptcha } = require("../helpers/recaptchaMiddleware");

const { paymentStatusToString } = require("../helpers/paymentStatusToString");

const { createPartPaymentOrder } = require("../helpers/liqpayPartPayment");

router.post("/order", addRequestHistory, recaptcha, async (req, res) => {
    const errors = orderSchema.validate(req.body).error;

    if (errors) {
        res.json({ status: 400, errors });
    } else {
        const {
            payer,
            receiver,
            customReceiver,
            shippingMethod,
            deliveryAddress,
            paymentMethod,
            paymentService,
            items,
            city,
            promoName,
            useBonuses,
        } = req.body;

        let promo = {};

        try {
            if (promoName) {
                promo = await Promo.findOne({ promoName });
            }
        } catch {
            console.log("Couldn't find promo");
        }

        let customer = { _id: undefined };

        if (req.customer) {
            try {
                customer = await Customer.findOne({
                    _id: req.customer.customerId,
                });
            } catch {}
        }

        let bonusAmount = 0;

        if (useBonuses && customer && customer.bonuses) {
            bonusAmount = customer.bonuses;
        }

        const orderTotal = await calculateOrderTotal(
            items,
            shippingMethod,
            paymentMethod,
            promo,
            bonusAmount
        );
        if (customer && customer.bonuses && bonusAmount) {
            await customer.update({
                bonuses: bonusAmount - orderTotal.usedBonuses,
            });
        }

        let order;

        try {
            order = await Order.create({
                shippingMethod,
                shippingAddress: deliveryAddress,
                city,
                payer,
                receiver: receiver === "custom" ? customReceiver : payer,
                customReceiver: receiver === "custom",
                paymentMethod,
                paymentService,
                items: orderTotal.items,
                paymentStatus:
                    paymentMethod === ("card" || "partPayment")
                        ? "pending"
                        : "accepted",
                deliveryStatus: "",
                customer: customer._id,
                amount:
                    orderTotal.sum -
                    orderTotal.discount -
                    orderTotal.usedBonuses,
                discount: orderTotal.discount,
                promo,
                usedBonuses: orderTotal.usedBonuses,
            });

            setTimeout(async () => {
                const moyskladCallback = await createMoyskladOrder(order);
                order.moyskladId = moyskladCallback.id;
                order.save();
            }, 0);

            await order.save();
        } catch (err) {
            console.log(err);
            return res.json({ status: 400, err: "Couldn't create order" });
        }

        try {
            customer.orders.push(order);
            await customer.save();
        } catch {}

        if (paymentMethod === "cash") {
            return res.json({
                fields: {},
                orderId: order.uuid,
                amount: order.amount,
            });
        }

        if (paymentService === "wayforpay") {
            const productName = orderTotal.items.map((item) => item.title);
            const productCount = orderTotal.items.map((item) => item.quantity);
            const productPrice = orderTotal.items.map((item) => item.price);

            if (orderTotal.discount) {
                productName.push("Скидка");
                productCount.push(1);
                productPrice.push(-orderTotal.discount);
            }

            const wayforpay = await Wayforpay.findOne({});

            const data = {
                merchantAccount: wayforpay.account,
                merchantDomainName: process.env.WAYFORPAY_DOMAIN_NAME,
                orderReference: order.uuid,
                orderDate: new Date(order.date).getTime(),
                amount: order.amount,
                currency: "UAH",
                productName,
                productCount,
                productPrice,
            };

            const keysForSignature = [
                "merchantDomainName",
                "orderReference",
                "orderDate",
                "amount",
                "currency",
                "productName",
                "productCount",
                "productPrice",
            ];

            let text = data["merchantAccount"];

            for (const key of keysForSignature) {
                if (Array.isArray(data[key])) {
                    text += `;${data[key].join(";")}`;
                } else {
                    text += `;${data[key]}`;
                }
            }

            const hash = crypto
                .createHmac("md5", wayforpay.private)
                .update(text)
                .digest("hex");

            res.json({
                fields: {
                    ...data,
                    merchantSignature: hash,
                    serviceUrl: `${process.env.PAYMENT_HANDLER_SERVER}/orderCheckValidity/wayforpay`,
                    returnUrl: `${process.env.REACT_APP}/orderResult/wayforpay`,
                },
                action: "https://secure.wayforpay.com/pay",
                orderId: order.uuid,
                method: "POST",
            });
        }

        if (paymentService === "liqpay") {
            const liqpayKeys = await Liqpay.findOne({});

            const liqpay = new LiqPay(liqpayKeys.public, liqpayKeys.private);

            res.json({
                ...liqpay.cnb_object({
                    action: "pay",
                    amount: order.amount,
                    currency: "UAH",
                    description: orderTotal.items.reduce(
                        (acc, item) =>
                            `${acc} ${item.title} x${item.quantity}\n`,
                        ""
                    ),
                    order_id: order.uuid,
                    version: 3,
                    server_url: `${process.env.PAYMENT_HANDLER_SERVER}/orderCheckValidity/liqpay`,
                    result_url: `${process.env.PAYMENT_HANDLER_SERVER}/orderResult/liqpay`,
                }),
                orderId: order.uuid,
                method: "POST",
            });
        }

        if (paymentMethod === "partPayment") {
            const token = await createPartPaymentOrder(order);

            res.json({
                action: `https://payparts2.privatbank.ua/ipp/v2/payment?token=${token}`,
                method: "GET",
                fields: {},
                orderId: order.uuid,
            });
        }
    }
});

router.post("/orderCheckValidity/wayforpay", async (req, res) => {
    const [paymentStatus, amount] = await checkWayforpayValidity(req);

    const orderString = paymentStatusToString(paymentStatus);

    if (
        orderString === "success" ||
        orderString === "pending" ||
        orderString === "processing"
    ) {
        return res.redirect(
            `${process.env.REACT_APP}/#order-success?amount=${amount}`
        );
    } else {
        return res.redirect(`${process.env.REACT_APP}/`);
    }
});

router.post("/orderCheckValidity/liqpay", async (req, res) => {
    console.log("ordercheckvalidity", req.body);

    try {
        const [paymentStatus] = await checkLiqpayValidity(req);

        const orderString = paymentStatusToString(paymentStatus);

        if (
            orderString === "success" ||
            orderString === "pending" ||
            orderString === "processing"
        ) {
            return res.redirect(`${process.env.REACT_APP}`);
        }
    } catch (error) {
        console.error(error);
    }

    return res.redirect(`${process.env.REACT_APP}/`);
});

router.post("/orderCheckValidity/partPayment", async (req, res) => {
    const paymentStatus = await checkPartPaymentValidity(req);

    const orderString = paymentStatusToString(paymentStatus);

    if (
        orderString === "success" ||
        orderString === "pending" ||
        orderString === "processing"
    ) {
        return res.redirect(`${process.env.REACT_APP}/#order-success`);
    } else {
        return res.redirect(`${process.env.REACT_APP}/`);
    }
});

router.post("/orderResult/liqpay", async (req, res) => {
    const [paymentStatus, amount] = await checkLiqpayValidity(req);
    const statusString = paymentStatusToString(paymentStatus);

    if (
        statusString === "success" ||
        statusString === "pending" ||
        statusString === "processing"
    ) {
        return res.redirect(
            `${process.env.REACT_APP}/#order-success?amount=${amount}`
        );
    } else {
        return res.redirect(`${process.env.REACT_APP}/`);
    }
});

router.post("/orderResult/wayforpay", async (req, res) => {
    const [paymentStatus, amount] = await checkWayforpayValidity(req);
    const statusString = paymentStatusToString(paymentStatus);

    if (
        statusString === "success" ||
        statusString === "pending" ||
        statusString === "processing"
    ) {
        return res.redirect(
            `${process.env.REACT_APP}/#order-success?amount=${amount}`
        );
    } else {
        return res.redirect(`${process.env.REACT_APP}/`);
    }
});

module.exports = router;
