const Joi = require("joi");

const pattern = /^[+]?([0-9]{2})?[\s]?[(]?[\s]?[0-9]{3}[\s]?[)]?[\s]?[0-9]{3}[\s]?[-]?[\s]?[0-9]{2}[\s]?[-]?[\s]?[0-9]{2}[\s]?$/im;

module.exports.orderSchema = Joi.object().keys({
    payer: Joi.object()
        .keys({
            lastName: Joi.string().min(2).required(),
            firstName: Joi.string().min(2).required(),
            phone: Joi.string().regex(pattern).required(),
        })
        .required(),
    customReceiver: Joi.when("receiver", {
        is: "custom",
        then: Joi.object()
            .keys({
                lastName: Joi.string().min(2).required(),
                firstName: Joi.string().min(2).required(),
                phone: Joi.string().regex(pattern).required(),
                patronymic: Joi.string().min(3).required(),
            })
            .required(),
    }),
    receiver: Joi.string().valid("payer", "custom").required(),
    shippingMethod: Joi.string().valid("postOffice", "courier").required(),
    city: Joi.object()
        .keys({
            value: Joi.string(),
            name: Joi.string().required(),
            __typename: Joi.any(),
        })
        .required(),
    deliveryAddress: Joi.when("shippingMethod", {
        is: "postOffice",
        then: Joi.object()
            .keys({
                value: Joi.string().required(),
                name: Joi.string().required(),
            })
            .required(),
    })
        .when("shippingMethod", {
            is: "courier",
            then: Joi.object()
                .keys({
                    street: Joi.object()
                        .keys({
                            value: Joi.string().required(),
                            name: Joi.string().required(),
                        })
                        .required(),
                    appartment: Joi.string().allow(""),
                    houseNumber: Joi.string().required(),
                })
                .required(),
        })
        .required(),
    paymentMethod: Joi.string().valid("card", "cash", "partPayment").required(),
    paymentService: Joi.string().when("paymentMethod", {
        switch: [
            {
                is: "card",
                then: Joi.string().valid("wayforpay", "liqpay").required(),
            },
            {
                is: "cash",
                then: Joi.string().allow(""),
            },
            {
                is: "partPayment",
                then: Joi.string().allow(""),
            },
        ],
    }),
    partsCount: Joi.when("paymentMethod", {
        is: "partPayment",
        then: Joi.number().required(),
    }),
    items: Joi.array()
        .items(
            Joi.object().keys({
                prodUuid: Joi.string(),
                quantity: Joi.number(),
                size: Joi.string().allow(""),
            })
        )
        .required(),
    promoName: Joi.string().allow(""),
    useBonuses: Joi.boolean(),
    "g-recaptcha": Joi.string().allow(""),
});

module.exports.registerSchema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().min(7).required(),
    subscribe: Joi.boolean(),
    "g-recaptcha": Joi.string().allow(""),
});

module.exports.loginSchema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().min(7).required(),
    remember: Joi.boolean(),
    "g-recaptcha": Joi.string().allow(""),
});
