module.exports.paymentStatusToString = (paymentStatus) => {
    if (typeof paymentStatus !== "string") {
        return "error";
    }
    switch (paymentStatus.toLowerCase()) {
        case "error":
        case "failure":
        case "expired":
        case "declined":
            return "error";
        case "success":
        case "approved":
        case "wait_accept":
            return "success";
        case "inprocessing":
            return "processing";
        case "refunded/voided":
        case "reversed":
            return "returned";
        default:
            return "pending";
    }
};
