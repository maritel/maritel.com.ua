const RequestsFromIp = require("../models/requestsFromIp");

const addRequestHistory = async (req, _, next) => {
    const ip = (
        req.headers["x-forwarded-for"] ||
        req.connection.remoteAddress ||
        ""
    )
        .split(",")[0]
        .trim();

    const date = new Date();

    date.setMinutes(date.getMinutes() - 15);

    const requestCount = await RequestsFromIp.find({
        ip,
        date: { $gte: date },
    }).countDocuments();

    RequestsFromIp.create({ ip });
    if (requestCount > 5) {
        req.shouldCheckCaptcha = true;
    }
    return next();
};

module.exports = { addRequestHistory };
