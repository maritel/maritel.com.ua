const fetch = require("node-fetch");

const recaptchaMiddleware = async (req, res, next) => {
    req.captcha = {};

    if (req.shouldCheckCaptcha) {
        const token = req.body["g-recaptcha"];

        if (token) {
            const secretKey = process.env.CAPTCHA_PRIVATE;
            const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secretKey}&response=${token}`;
            const response = await fetch(url, { method: "POST" });
            const googleResponse = await response.json();

            req.captcha = googleResponse;

            if (!req.captcha.success) {
                return res.json({ error: "captcha" });
            }
        } else {
            return res.json({ error: "captcha" });
        }
    } else {
        req.captcha.success = true;
    }

    return next();
};

module.exports = { recaptcha: recaptchaMiddleware };
