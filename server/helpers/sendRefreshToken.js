module.exports.sendRefreshToken = (res, token) => {
    res.cookie("jid", token, {
        httpOnly: true,
        path: "/refresh_token",
        secure: process.env.NODE_ENV === "production",
    });
};
