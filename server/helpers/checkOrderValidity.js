const Order = require("../models/order");
const Liqpay = require("../models/liqpay");

const LiqPay = require("../helpers/liqpay.sdk");
const Wayforpay = require("../models/wayforpay");

const { updateMoyskladOrder } = require("../moysklad/order");

module.exports.checkLiqpayValidity = async (req) => {
    try {
        const data = req.body;

        const liqpayKeys = await Liqpay.findOne({});

        const liqpay = new LiqPay(liqpayKeys.public, liqpayKeys.private);

        const sign = liqpay.str_to_sign(
            liqpayKeys.private + data.data + liqpayKeys.private
        );

        if (sign === data.signature) {
            const json = JSON.parse(
                Buffer.from(data.data, "base64").toString()
            );

            await Order.updateOne(
                { uuid: json.order_id },
                {
                    paymentStatus: json.status,
                    paymentServiceOrderId: json.liqpay_order_id,
                    servicePaymentId: json.payment_id,
                }
            );

            const order = await Order.findOne({ uuid: json.order_id });

            updateMoyskladOrder(order);

            return [json.status, order.amount];
        }

        return ["error", 0];
    } catch (err) {
        return ["error", 0];
    }
};

module.exports.checkWayforpayValidity = async (req) => {
    try {
        let data = {};

        for (const key in req.body) {
            data = JSON.parse(key);
        }

        let text = data["merchantAccount"];
        const keysForSignature = [
            "orderReference",
            "amount",
            "currency",
            "authCode",
            "cardPan",
            "transactionStatus",
            "reasonCode",
        ];

        for (const key of keysForSignature) {
            text += `;${data[key]}`;
        }

        const wayforpay = await Wayforpay.findOne({});

        const hash = crypto
            .createHmac("md5", wayforpay.private)
            .update(text)
            .digest("hex");
        if (hash === data["merchantSignature"]) {
            await Order.updateOne(
                { uuid: data.orderReference },
                {
                    paymentStatus: data.transactionStatus,
                    paymentServiceOrderId: data.authCode,
                }
            );

            const order = await Order.findOne({ uuid: data.orderReference });

            updateMoyskladOrder(order);
            return [data.transactionStatus, order.amount];
        }

        return ["error", 0];
    } catch (err) {
        return ["error", 0];
    }
};
