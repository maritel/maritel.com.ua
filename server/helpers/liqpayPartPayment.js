const fetch = require("node-fetch");

const storeId = "4AAD1369CF734B64B70F";
const password = "75bef16bfdce4d0e9c0ad5a19b9940df";

const LiqpaySDK = require("./liqpay.sdk");

const Product = require("../models/product");
const Order = require("../models/order");

const createPartPaymentOrder = async (order) => {
    const liqpay = new LiqpaySDK();

    const productsFromOrder = await Product.find({
        uuid: { $in: order.items.map((item) => item.prodUuid) },
    });

    order.items = Array.from(order.items).map((item) => {
        const match = productsFromOrder.find(
            (product) => product.uuid === item.prodUuid
        );

        if (match) {
            return {
                quantity: item.quantity,
                size: item.size,
                name: match.title,
                price: match.price,
            };
        }

        return item;
    });

    const data = {
        storeId,
        orderId: order.uuid,
        amount: parseFloat(order.amount.toString()).toFixed(2),
        partsCount: 6,
        merchantType: "PP",
        products: order.items.map((item) => ({
            name: item.name,
            count: item.quantity,
            price: parseFloat(item.price.toString()).toFixed(2),
        })),
        responseUrl: `${process.env.PAYMENT_HANDLER_SERVER}/orderCheckValidity/partPayment`,
        redirectUrl: `${process.env.REACT_APP}`,
    };

    const productsString = data.products.reduce(
        (acc, { name, count, price }) => acc + name + count + price * 100,
        ""
    );

    const {
        orderId,
        amount,
        partsCount,
        merchantType,
        responseUrl,
        redirectUrl,
    } = data;

    const signature = liqpay.str_to_sign(
        password +
            storeId +
            orderId +
            amount * 100 +
            partsCount +
            merchantType +
            responseUrl +
            redirectUrl +
            productsString +
            password
    );

    data.signature = signature;

    const response = await fetch(
        `https://payparts2.privatbank.ua/ipp/v2/payment/create`,
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=UTF-8;",
                "Accept-Encoding": "UTF-8;",
                Accept: "application/json;",
            },
            body: JSON.stringify(data),
        }
    );

    const json = await response.json();

    if (json.state === "SUCCESS") {
        const { state, storeId, orderId, token, signature } = json;

        const responseSignature = liqpay.str_to_sign(
            password + state + storeId + orderId + token + password
        );

        if (responseSignature === signature) {
            return token;
        }

        throw new Error(`liqpayCreatePartPayment: signature error`);
    }

    if (json.state === "FAIL") {
        const { state, storeId, orderId, message, signature } = json;

        const responseSignature = liqpay.str_to_sign(
            password + state + storeId + orderId + message + password
        );

        if (responseSignature === signature) {
            throw new Error(`liqpayCreatePartPayment: ${json.message}`);
        }

        throw new Error(`liqpayCreatePartPayment: signature error`);
    }
};

const checkPartPaymentValidity = async (req) => {
    const liqpay = new LiqpaySDK();

    const { storeId, orderId, paymentState, message, signature } = req.body;

    console.log("hello", req.body);

    const signatureToCompare = liqpay.str_to_sign(
        password + storeId + orderId + paymentState + message + password
    );

    if (signature === signatureToCompare) {
        Order.updateOne(
            { uuid: orderId },
            { paymentStatus: paymentState.toLowerCase() }
        );
    }
};

module.exports = { createPartPaymentOrder, checkPartPaymentValidity };
