const getPaymentStatus = (paymentStatus) => {
    switch (paymentStatus.toLowerCase()) {
        case "error":
        case "failure":
        case "expired":
        case "declined":
            return "error";
        case "success":
        case "approved":
            return "success";
        case "inprocessing":
            return "processing";
        case "refunded/voided":
        case "reversed":
            return "refunded";
        default:
            return "pending";
    }
};


module.exports = {getPaymentStatus}