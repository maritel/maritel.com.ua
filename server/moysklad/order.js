const { moyskladFetch } = require("./index");
const Product = require("../models/product");

const { getPaymentStatus } = require("../helpers/getPaymentStatus");

const orderStateMetaSubscribe = {
    href:
        "https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/states/56ae4f35-32d7-11eb-0a80-042500254d03",
    metadataHref:
        "https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata",
    type: "state",
    mediaType: "application/json",
};

const getSettings = async () => {
    const response = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/context/usersettings"
    );
    return response.json();
};

const generateDescription = (order) => {
    const paymentStatus = getPaymentStatus(order.paymentStatus);

    let str = "";

    if (order.customReceiver) {
        const { firstName, lastName, patronymic, phone } = order.receiver;

        str += `
            Получатель: ${lastName} ${firstName} ${patronymic}
            Телефон получателя: ${phone}
        `;
    }

    if (order.paymentMethod === "card") {
        str += `
            Оплата: ${order.paymentService}
            Оплачено: ${
                paymentStatus === "success" ? `${order.amount} грн.` : "0 грн."
            }
        `;
    }

    str = str.replace(/^\s*/gm, "");

    return str;
};
const requiredAttributes = [
    { name: "Вид оплты" },
    { name: "Промо-код" },
    { name: "Служба доставки", searchValue: "NOVAPOSHTA" },
    { name: "Менеджер по продажам", searchValue: "Админ" },
    { name: "Вид заказа", searchValue: "Обычный заказ" },
];

const getAttributeCustomEntityValue = async (
    attribute,
    { paymentMethod, promo }
) => {
    const idx = requiredAttributes.findIndex(
        (item) => item.name.toLowerCase() === attribute.name.toLowerCase()
    );

    if (~idx) {
        let customEntity;
        let customEntityData;
        if (attribute.type === "customentity") {
            const customEntityId = attribute.customEntityMeta.href.match(
                /[^/]{2,}$/
            )[0];

            customEntity = await moyskladFetch(
                `https://online.moysklad.ru/api/remap/1.2/entity/customentity/${customEntityId}`
            );
        }

        if (customEntity) {
            customEntityData = await customEntity.json();
        }

        switch (attribute.name) {
            case "Вид оплты":
                let paymentType;

                if (paymentMethod === "cash") {
                    paymentType = customEntityData.rows.find(
                        (item) =>
                            item.name.toString().toLowerCase() ===
                            "наложенный платеж"
                    );
                } else {
                    paymentType = customEntityData.rows.find(
                        (item) =>
                            item.name.toString().toLowerCase() ===
                            "полная оплата"
                    );
                }
                return { ...attribute, value: paymentType };
            case "Промо-код":
                let promoName = "";
                if (promo) {
                    promoName = promo.promoName;
                }
                return { ...attribute, value: promoName };
            default:
                const result = customEntityData.rows.find(
                    (item) =>
                        item.name.toString().toLowerCase() ===
                        requiredAttributes[idx].searchValue.toLowerCase()
                );
                return { ...attribute, value: result };
        }
    }
};

const createCounterparty = async (order) => {
    const name = `${order.payer.lastName} ${order.payer.firstName}`;

    let address = "";

    if (order.shippingMethod === "courier") {
        const { street, houseNumber, appartment } = order.shippingAddress;

        address = `${street.name} ${houseNumber} ${
            appartment ? `, кв. ${appartment}` : ""
        }`;
    } else {
        address = `${order.city.name}, ${order.shippingAddress.name}`;
    }

    let description = "";

    if (order.customReceiver) {
        description =
            "Данные получателя: " +
            order.receiver.lastName +
            order.receiver.firstName +
            order.receiver.patronymic +
            "\n";
        description += "Телефон получателя: " + order.receiver.phone;
    }

    const response = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/entity/counterparty",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                name,
                description,
                companyType: "individual",
                actualAddress: address,
                phone: order.payer.phone,
            }),
        }
    );

    const json = await response.json();

    return json;
};

const createCounterpartySubscriber = async (subscriber) => {
    const { email, phone } = subscriber;

    let { name } = subscriber;

    if (!name) {
        name = "Не указано";
    }

    const response = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/entity/counterparty",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                name,
                email,
                phone,
                companyType: "individual",
            }),
        }
    );

    const json = await response.json();

    return json;
};

const createMoyskladOrderSubscriber = async (subscriber) => {
    const counterPartyPromise = createCounterpartySubscriber(subscriber);

    const attributesResponse = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/attributes"
    );
    const attributeList = await attributesResponse.json();

    let attibuteListWithValues = [];

    if (attributeList.rows && attributeList.rows.length) {
        for (const item of attributeList.rows) {
            const idx = requiredAttributes.findIndex(
                (el) => el.name.toLowerCase() === item.name.toLowerCase()
            );

            if (~idx) {
                attibuteListWithValues.push(
                    getAttributeCustomEntityValue(item, {
                        paymentMethod: null,
                    })
                );
            }
        }
    }

    attibuteListWithValues = await Promise.all(attibuteListWithValues);

    const data = await getSettings();

    const positions = [];

    const { product, size: selectedSize } = subscriber;

    let meta = {};

    if (product && product.moyskladMeta && product.moyskladMeta.href) {
        meta = product.moyskladMeta;
    } else if (product && selectedSize) {
        const sizeList = JSON.parse(product.sizes);

        const size = sizeList.find((el) => el.size === selectedSize);

        if (size) {
            meta = size.moyskladMeta;
        }
    } else {
        console.error("couldn't find product from order");
    }

    positions.push({
        quantity: 1,
        price: product.price * 100,
        vat: 0,
        assortment: {
            meta,
        },
    });

    const counterParty = await counterPartyPromise;

    const response = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/entity/customerorder",
        {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify({
                organization: data.defaultCompany,
                agent: counterParty,
                positions,
                attributes: attibuteListWithValues,
                state: { meta: orderStateMetaSubscribe },
            }),
        }
    );

    const json = await response.json();

    return json;
};

const createMoyskladOrder = async (order) => {
    const getCounterParty = createCounterparty(order);

    const attributesResponse = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/entity/customerorder/metadata/attributes"
    );
    const attributeList = await attributesResponse.json();

    let attibuteListWithValues = [];

    if (attributeList.rows && attributeList.rows.length) {
        for (const item of attributeList.rows) {
            const idx = requiredAttributes.findIndex(
                (el) => el.name.toLowerCase() === item.name.toLowerCase()
            );

            if (~idx) {
                if (
                    (!order.promo || !order.promo.promoName) &&
                    item.name === "Промо-код"
                ) {
                } else {
                    attibuteListWithValues.push(
                        getAttributeCustomEntityValue(item, order)
                    );
                }
            }
        }
    }

    attibuteListWithValues = await Promise.all(attibuteListWithValues);

    const data = await getSettings();

    const positions = [];

    const totalItemQuantity = order.items.reduce(
        (acc, item) => acc + item.quantity,
        0
    );

    const itemDiscount =
        (order.discount / (order.amount + order.discount) / totalItemQuantity) *
        100;

    for (const item of order.items) {
        const product = await Product.findOne({ uuid: item.prodUuid });

        let meta = {};

        if (product && product.moyskladMeta && product.moyskladMeta.href) {
            meta = product.moyskladMeta;
            positions.push({
                quantity: item.quantity,
                price: product.price * 100,
                discount: itemDiscount * item.quantity,
                vat: 0,
                assortment: {
                    meta,
                },
                reserve: item.quantity,
            });
        } else if (product && item.size) {
            const sizeList = JSON.parse(product.sizes);

            const size = sizeList.find((el) => el.size === item.size);

            if (size) {
                meta = size.moyskladMeta;
            }

            positions.push({
                quantity: item.quantity,
                price: product.price * 100,
                discount: itemDiscount * item.quantity,
                vat: 0,
                assortment: {
                    meta,
                },
                reserve: item.quantity,
            });
        } else {
            console.error("couldn't find product from order");
        }
    }

    const counterParty = await getCounterParty;

    const response = await moyskladFetch(
        "https://online.moysklad.ru/api/remap/1.2/entity/customerorder",
        {
            headers: { "Content-Type": "application/json" },
            method: "POST",
            body: JSON.stringify({
                organization: data.defaultCompany,
                agent: counterParty,
                positions,
                attributes: attibuteListWithValues,
                description: generateDescription(order),
            }),
        }
    );

    const json = await response.json();

    if (json.errors) {
        console.error("createmoyskaldorder error: ", json);
    }

    return json;
};

const updateMoyskladOrder = async (order) => {
    const description = generateDescription(order);

    const response = await moyskladFetch(
        `https://online.moysklad.ru/api/remap/1.2/entity/customerorder/${order.moyskladId}`,
        {
            method: "PUT",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ description }),
        }
    );

    return response.json();
};

const getMoyskladOrder = async (order) => {
    const response = await moyskladFetch(
        `https://online.moysklad.ru/api/remap/1.2/entity/customerorder/${order.moyskladId}`
    );
    return response.json();
};

module.exports = {
    createMoyskladOrder,
    createMoyskladOrderSubscriber,
    updateMoyskladOrder,
    generateDescription,
    getMoyskladOrder,
};
