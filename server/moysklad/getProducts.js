const { moyskladFetch } = require("./index");
const MoyskladProduct = require("../models/moyskladProducts");
const MoyskladUpdate = require("../models/moyskladUpdate");

const getProducts = async () => {
    let url = `https://online.moysklad.ru/api/remap/1.2/entity/product`;

    const response = await moyskladFetch(url);

    const productList = await response.json();

    if (productList.rows && productList.rows.length) {
        const bulk = MoyskladProduct.collection.initializeOrderedBulkOp();

        for (const product of productList.rows) {
            const article = product.name.match(/\S*/)[0];

            bulk.find({ moyskladId: product.id })
                .upsert()
                .updateOne({
                    $set: { moyskladId: product.id, article },
                });
        }

        await bulk.execute();

        await MoyskladUpdate.create({ date: Date.now() });
    }

    return;
};

module.exports = { getProducts };
