const MoySklad = require("../models/moysklad");
const btoa = require("btoa");
const fetch = require("node-fetch");

const sleep = (time) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve("");
        }, time);
    });
};

function b64EncodeUnicode(str) {
    return btoa(
        encodeURIComponent(str).replace(
            /%([0-9A-F]{2})/g,
            function toSolidBytes(_, p1) {
                return String.fromCharCode(+`0x${p1}`);
            }
        )
    );
}

const generateHeader = async () => {
    const credentials = await MoySklad.findOne({});

    if (!credentials) {
        throw new Error("No moysklad user found");
    }

    return {
        Authorization: `Basic ${b64EncodeUnicode(
            `${credentials.login}:${credentials.password}`
        )}`,
    };
};

const moyskladFetch = async (url = "", options = {}) => {
    let auth = "";
    if (!this.moyskladAuth) {
        this.moyskladAuth = { loading: true, auth: generateHeader() };
        auth = await this.moyskladAuth.auth;
        this.moyskladAuth.loading = false;
    } else {
        auth = await this.moyskladAuth.auth;
    }

    const headers = options.headers ? { ...options.headers, ...auth } : auth;

    options = { ...options, headers };

    let fetchDepth = 0;

    let success = false;

    let result;

    try {
        while (!success && fetchDepth < 4) {
            fetchDepth++;
            const response = await fetch(url, options);

            result = await response.json();

            if (!result.errors) {
                success = true;
            }

            if (!success) {
                await sleep(fetchDepth * 1000);
            }
        }
    } catch (err) {
        throw new Error(`
Moysklad fetch error. 
Moysklad fetch URL: ${url}
Error: ${JSON.stringify(err, 0, 2)}
        `);
    }

    if (result.errors) {
        throw new Error(`
Moysklad fetch error. 
Moysklad fetch URL: ${url}. 
Error: ${JSON.stringify(result.errors, 0, 2)}
        `);
    }

    return { ...result, json: () => new Promise((resolve) => resolve(result)) };
};

const toTimeStandard = (number) => {
    const str = number.toString();
    return str.length === 1 ? `0${str}` : str;
};

const dateToTimeString = (date) => {
    date.setHours(date.getHours() + 1);

    const year = date.getFullYear();
    const month = toTimeStandard(date.getMonth() + 1);
    const day = toTimeStandard(date.getDate());

    const hours = toTimeStandard(date.getHours());
    const minutes = toTimeStandard(date.getMinutes());
    const seconds = toTimeStandard(date.getSeconds());

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
};

module.exports = { generateHeader, moyskladFetch, dateToTimeString };
