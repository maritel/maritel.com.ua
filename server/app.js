require("dotenv").config();
const { graphqlHTTP } = require("express-graphql");
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const schema = require("./schema/schema");
const app = express();
const PORT = process.env.PORT || 6000;
const orderRouter = require("./routes/order");
const authRouter = require("./routes/auth");
const amocrmRouter = require("./routes/amocrm");

const isAuth = require("./helpers/isAuth");

app.use(express.static("prod"));

if (process.env.HEROKU) {
    app.use(express.static("build"));
}

const origin = ["https://maritel.com.ua", "https://www.maritel.com.ua"];

if (process.env.NODE_ENV !== "production") {
    origin.push(process.env.REACT_APP);
}

app.use(cors({ credentials: true, origin }));

app.use(isAuth);

app.use(cookieParser());
app.use(express.json({ extended: true }));
app.use(express.urlencoded({ extended: true }));

app.use(orderRouter);
app.use(authRouter);
app.use("/amocrm", amocrmRouter);

mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
});

app.use(
    "/graphql",
    graphqlHTTP({
        schema,
        graphiql: true,
    })
);

const dbContection = mongoose.connection;
dbContection.on("error", (err) => console.log(`Contection error: ${err}`));
dbContection.once("open", () => console.log("Connected to DB"));

app.listen(PORT, (err) => {
    err ? console.log(err) : console.log("Server started!");
});
