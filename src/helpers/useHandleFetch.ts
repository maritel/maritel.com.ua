import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    setCaptchaPopupState,
    setCaptchaShouldRefetch,
} from "../store/actionCreators";
import { getCaptchaShouldRefetch } from "../store/actionsTypes";

interface StateType<R> {
    loading: boolean;
    data: R;
    haveResult: boolean;
}

export const useHandleFetch = <R>(
    callback: () => Promise<R | CaptchaError>
): [StateType<R>, () => void] => {
    const dispatch = useDispatch();

    const shouldRefetch = useSelector(getCaptchaShouldRefetch);

    const [send, setSend] = useState(false);

    const [loading, setLoading] = useState(false);

    const [data, setData] = useState({
        loading: false,
        data: {} as R,
        haveResult: false,
    } as StateType<R>);

    const handleCallback = useCallback(async () => {
        setSend(false);
        setLoading(true);
        const response = await callback();

        if ((response as CaptchaError).error === "captcha") {
            dispatch(setCaptchaShouldRefetch(false));
            dispatch(setCaptchaPopupState(true));
            setSend(true);
        } else {
            setData({
                loading: false,
                haveResult: true,
                data: response as R,
            });
        }

        setLoading(false);
    }, [callback, dispatch]);

    useEffect(() => {
        if (send && !loading && shouldRefetch) {
            setData({ loading: true, haveResult: false, data: {} as R });
            handleCallback();
        }
    }, [handleCallback, loading, send, shouldRefetch]);

    return [
        data,
        () => {
            if (send) {
                setSend(false);
                setTimeout(() => {
                    setSend(true);
                }, 0);
            } else {
                setSend(true);
            }
            setTimeout(() => {
                dispatch(setCaptchaShouldRefetch(true));
            }, 0);
        },
    ];
};

// export const useHandleFetch1 = <R>(
//     callback: () => Promise<R | CaptchaError>
// ): [StateType<R>, () => void] => {
//     const dispatch = useDispatch();

//     const captcha = useSelector(getCaptchaValue);
//     const shouldRefetch = useSelector(getCaptchaShouldRefetch);

//     const [hello, setHello] = useState(() => initialState());
//     const [loading, setLoading] = useState(false);

//     const [state, setState] = useState({
//         loading: false,
//         data: {} as R,
//         haveResult: false,
//         success: false,
//         captchaError: false,
//     });

//     const isCurrent = useRef(true);

//     const handleResponse = useCallback(async () => {
//         setLoading(true);
//         const result = await callback();
//         console.log("parsing", result);
//         if (isCurrent.current) {
//             if ((result as CaptchaError).error === "captcha") {
//                 console.log("captcha error");
//                 dispatch(setCaptchaPopupState(true));
//                 dispatch(setCaptchaShouldRefetch(false));
//                 if (!captcha && !state.captchaError) {
//                     setState({
//                         loading: true,
//                         data: {} as R,
//                         haveResult: false,
//                         success: false,
//                         captchaError: true,
//                     });
//                 }
//             } else {
//                 console.log("data came through");

//                 setState({
//                     loading: false,
//                     data: result as R,
//                     haveResult: true,
//                     success: true,
//                     captchaError: false,
//                 });
//             }
//             setLoading(false);
//         }
//     }, [callback, captcha, dispatch, state.captchaError]);

//     useEffect(() => {
//         return () => {
//             isCurrent.current = false;
//         };
//     }, []);

//     useEffect(() => {
//         if (!loading && hello && state.haveResult) {
//             console.log("changing");
//             setState((prev) => {
//                 if (prev.captchaError) {
//                     dispatch(setCaptchaPopupState(true));
//                 }
//                 return {
//                     loading: false,
//                     data: {} as R,
//                     haveResult: false,
//                     success: false,
//                     captchaError: prev.captchaError,
//                 };
//             });
//         }
//     }, [dispatch, loading, hello, state.haveResult]);

//     useEffect(() => {
//         if (hello && state.captchaError) {
//             dispatch(setCaptchaPopupState(true));
//         }
//     }, [dispatch, hello, state.captchaError]);

//     useEffect(() => {
//         console.log("send", hello);
//         console.log("state", state.success, shouldRefetch, loading);
//         if (hello && callback && !state.success && shouldRefetch && !loading) {
//             handleResponse();
//         }
//     }, [
//         callback,
//         hello,
//         dispatch,
//         state,
//         captcha,
//         loading,
//         shouldRefetch,
//         handleResponse,
//     ]);

//     return [
//         state,
//         () => {
//             setHello(true);
//             dispatch(setCaptchaShouldRefetch(true));
//         },
//     ];
// };
