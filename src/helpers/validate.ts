export const validatePhone = (phone: number | string) => {
    const regex = /^[+]?([0-9]{2})?[\s]?[(]?[\s]?[0-9]{3}[\s]?[)]?[\s]?[0-9]{3}[\s]?[-]?[\s]?[0-9]{2}[\s]?[-]?[\s]?[0-9]{2}[\s]?$/im;

    return regex.test(phone.toString());
};
