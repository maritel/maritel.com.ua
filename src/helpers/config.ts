export const FOOTER_INFO: FooterInfo[] = [
    {
        name: "Помощь",
        fields: [
            {
                name: "Размерная сетка",
                link: "/sizeChart",
            },
            {
                name: "Список желаний",
                link: "/wishlist",
            },
            {
                name: "Мой аккаунт",
                link: "/account",
            },
        ],
    },
    {
        name: "О нас",
        fields: [
            {
                name: "О бренде",
                link: "/about/brand",
            },
            {
                name: "Блоггеры",
                link: "/about/bloggers",
            },
            {
                name: "Вакансии",
                link: "/about/vacancies",
            },
        ],
    },
    {
        name: "Правила",
        fields: [
            {
                name: "Доставка и оплата",
                link: "/about/delivery-policies",
            },
            {
                name: "Обмен и возврат",
                link: "/about/return-policies",
            },
            {
                name: "Правила сайта",
                link: "/about/rules",
            },
            {
                name: "Договор публичной оферты",
                link: "/about/public-offer",
            },
        ],
    },
];

export const INFO_SLIDER: string[] = [
    "БЕСПЛАТНАЯ ДОСТАВКА ПРИ ЗАКАЗЕ НА СУММУ ОТ 1500 ГРН",
    "БЕСПЛАТНАЯ ДОСТАВКА ПРИ ЗАКАЗЕ НА СУММУ ОТ 1501 ГРН",
    "БЕСПЛАТНАЯ ДОСТАВКА ПРИ ЗАКАЗЕ НА СУММУ ОТ 1502 ГРН",
];

export const splitValue = " / ";
export const sortBy = "sortBy";

export const SIZES_CONFIG: string[] = [
    "XXS",
    "XS",
    "S",
    "M",
    "L",
    "XL",
    "XXL",
    "XXXL",
];

export const SHOES_SIZES_CONFIG: string[] = [
    "35",
    "36",
    "37",
    "38",
    "39",
    "40",
    "41",
    "42",
    "43",
    "44",
    "45",
    "46",
    "47",
];

export const INFO_PAGES_MENU = [
    {
        title: "О НАС",
        items: [
            { name: "О бренде", link: "/about/brand" },
            { name: "Блоггеры", link: "/about/bloggers" },
            { name: "Вакансии", link: "/about/vacancies" },
        ],
    },
    {
        title: "ПРАВИЛА",
        items: [
            { name: "Доставка и оплата", link: "/about/delivery-policies" },
            { name: "Обмен и возврат", link: "/about/return-policies" },
            { name: "Правила сайта", link: "/about/rules" },
            {
                name: "Договор публичной оферты",
                link: "/about/public-offer",
            },
        ],
    },
];

export const BLOGGERS = [
    {
        name: "АННА АЛХИМ",
        at: "@anna_alkhim",
        subscriberCount: "538.000 - Подписчиков",
        postLink: "https://www.instagram.com/p/Bj8G3DYFoWu/",
    },
    {
        name: "SASHA PUSTOVIT",
        at: "@sashaabo",
        subscriberCount: "1.100.000 - Подписчиков",
        postLink:
            "https://www.instagram.com/p/BY3hz9KhNBD/?igshid=z76gealfyhkm",
    },
    {
        name: "АЛИНА ФРЕНДИ",
        at: "@alina_frendiy",
        subscriberCount: "386.000 - Подписчиков",
        postLink: "https://www.instagram.com/p/BgJESHLB-cb/",
    },
    {
        name: "NASTYA STADNYK",
        at: "@n.stadnyk",
        subscriberCount: "945.000 - Подписчиков",
        postLink:
            "https://www.instagram.com/p/BaB-5wTBzho/?igshid=gf14hjo5lak4",
    },
    {
        name: "ДАРИНА ЗАПЕСОЧНАЯ",
        at: "@darina_zapesochnaya",
        subscriberCount: "197.000 - Подписчиков",
        postLink: "https://www.instagram.com/p/BuoUlV2AxLz/",
    },
    {
        name: "ЛЮДА З ГОЛІВУДА",
        at: "@lyudashollywooda",
        subscriberCount: "164.000 - Подписчиков",
        postLink:
            "https://www.instagram.com/p/BlQEkLNlvHn/?igshid=1ad08nm5i222k",
    },
    {
        name: "КСЮША",
        at: "@kseniia_nikolic",
        subscriberCount: "204.000 - Подписчиков",
        postLink: "https://www.instagram.com/p/BrCnsutBizk/",
    },
    {
        name: "MARINA MANIUNENKO",
        at: "@marina_ricci",
        subscriberCount: "284.000 - Подписчиков",
        postLink: "https://www.instagram.com/p/BlFSPwthR0G/",
    },
    {
        name: "СОФИЯ СТУЖУК",
        at: "@sofia_stuzhuk",
        subscriberCount: "1.400.000 - Подписчиков",
        postLink: "https://www.instagram.com/sofia_stuzhuk/",
    },
    {
        name: "OBRI",
        at: "@lia_obri",
        subscriberCount: "139.000 - Подписчиков",
        postLink: "https://www.instagram.com/lia_obri/",
    },
    {
        name: "ШВАЙКА ЮЛІЯ",
        at: "@shvayka_yuliya",
        subscriberCount: "47.000 - Подписчиков",
        postLink: "https://www.instagram.com/p/BenrIeXgxA8/",
    },
    {
        name: "JULIA BLACK",
        at: "@julia_black",
        subscriberCount: "46.000 - Подписчиков",
        postLink: "https://www.instagram.com/julia___black___/",
    },
];

export const DEFAULT_PAGE_TITLE =
    "Maritel’ - Женская одежда собственного производства";
