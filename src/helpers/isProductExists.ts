import { Wishlist } from "../store/wishList";

export const isProductExists = (
    productList: Products[],
    wishlist: Wishlist["items"],
    cart: CartProd[]
): Promise<{ wishlist: Wishlist["items"]; cart: CartProd[] }> => {
    return new Promise((resolve, reject) => {
        try {
            const result: { wishlist: Wishlist["items"]; cart: CartProd[] } = {
                wishlist: [],
                cart: [],
            };

            for (const product of productList) {
                const wishlistIndex = wishlist.findIndex(
                    (item) => item.prodId === product.uuid
                );

                if (~wishlistIndex) {
                    result.wishlist.push(wishlist[wishlistIndex]);
                }

                const cartIndex = cart.findIndex(
                    (item) => item.prodUuid === product.uuid
                );

                if (~cartIndex) {
                    result.cart.push(cart[cartIndex]);
                }
            }
            return resolve(result);
        } catch (err) {
            reject(err);
            throw new Error(err);
        }
    });
};
