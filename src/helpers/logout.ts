export const logout = async (dispatch: () => void): Promise<void> => {
    try {
        const res = await fetch(`${process.env.REACT_APP_SERVER}/logout`, {
            credentials: "include",
        });
        const json: { ok: boolean; accessToken?: "" } = await res.json();
        if (json.ok) {
            dispatch();
            window.location.href = `${window.location.protocol}//${window.location.host}`;
        }
    } catch (error) {
        console.log("logout error: ", error);
    }
};
