import { Action } from "redux";
import { SET_ADD_TO_CART_ERROR } from "./actions";

type SetError = Action<typeof SET_ADD_TO_CART_ERROR> & {
    payload: boolean;
};

type ActionsType = SetError;

const reducer = (state = false, action: ActionsType): boolean => {
    switch (action.type) {
        case SET_ADD_TO_CART_ERROR:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;
