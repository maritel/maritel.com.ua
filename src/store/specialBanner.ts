import { SET_SPECIAL_BANNER } from "./actions";
import { Action } from "redux";

type SetBanner = Action<typeof SET_SPECIAL_BANNER> & {
    payload: BannerSpecial;
};

type Actions = SetBanner;

const reducer = (state = null as null | BannerSpecial, action: Actions) => {
    switch (action.type) {
        case SET_SPECIAL_BANNER:
            return { ...action.payload };
        default:
            return state;
    }
};

export default reducer;
