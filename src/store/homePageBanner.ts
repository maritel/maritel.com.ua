import { SET_HOME_PAGE_BANNER } from "./actions";
import { Action } from "redux";

type ActionType = Action<typeof SET_HOME_PAGE_BANNER> & {
    payload: BannerHomePage;
};

const defaultState: BannerHomePage = {};

const reducer = (state = defaultState, actions: ActionType) => {
    switch (actions.type) {
        case SET_HOME_PAGE_BANNER:
            return { ...actions.payload };
        default:
            return state;
    }
};

export default reducer;
