import { Action } from "redux";
import { SET_BONUSES, SET_USE_BONUSES } from "./actions";

type SetUseBonuses = Action<typeof SET_USE_BONUSES> & {
    payload: boolean;
};

type SetBonuses = Action<typeof SET_BONUSES> & {
    payload: number;
};

type ActionsType = SetUseBonuses | SetBonuses;

type BonusesState = {
    useBonuses: boolean;
    bonuses: number;
};

const defaultState: BonusesState = { useBonuses: false, bonuses: 0 };

const reducer = (state = defaultState, action: ActionsType): BonusesState => {
    switch (action.type) {
        case SET_USE_BONUSES:
            return { ...state, useBonuses: action.payload };
        case SET_BONUSES:
            return { ...state, bonuses: action.payload };
        default:
            return state;
    }
};

export default reducer;
