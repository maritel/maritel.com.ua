import { Action } from "redux";
import {
    SET_CAPTCHA_POPUP_STATE,
    SET_CAPTCHA_SHOULD_REFETCH,
    SET_CAPTCHA_VALUE,
} from "./actions";

type SetPopupState = Action<typeof SET_CAPTCHA_POPUP_STATE> & {
    payload: boolean;
};

type SetValue = Action<typeof SET_CAPTCHA_VALUE> & {
    payload: string;
};

type SetShouldRefetch = Action<typeof SET_CAPTCHA_SHOULD_REFETCH> & {
    payload: boolean;
};

type ActionsType = SetPopupState | SetValue | SetShouldRefetch;

type CaptchaState = {
    popupOpen: boolean;
    value: string;
    shouldRefetch: boolean;
};

const reducer = (
    state = {
        popupOpen: false,
        value: "",
        shouldRefetch: true,
    } as CaptchaState,
    action: ActionsType
): CaptchaState => {
    switch (action.type) {
        case SET_CAPTCHA_POPUP_STATE:
            return { ...state, popupOpen: action.payload };
        case SET_CAPTCHA_VALUE:
            return { ...state, value: action.payload };
        case SET_CAPTCHA_SHOULD_REFETCH:
            return { ...state, shouldRefetch: action.payload };
        default:
            return state;
    }
};

export default reducer;
