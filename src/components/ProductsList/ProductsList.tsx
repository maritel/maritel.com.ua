import cn from "classnames";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useQuery } from "react-apollo";
import { useSelector } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";
import {
    DEFAULT_PAGE_TITLE,
    getCartProds,
    getColorsQuery,
    productsByCategory,
    productsFromSpecialCategory,
    sortBy,
} from "../../helpers";
import { handleDecode, handleTranslit } from "../../helpers/links";
import { prodListGoods } from "../../helpers/prodsListGoods";
import * as prodsSettings from "../../helpers/prodSort";
import * as types from "../../store/actionsTypes";
import { FilterBy } from "../FilterBy";
import { ProductCard } from "../ProductCard";
import { SelectDropDown } from "../SelectDropDown";
import "./ProductsList.scss";
import { Pagination } from "../Pagination";
import { SpinnerLoader } from "../SpinnerLoader";
import { Breadcrumbs } from "../common/Breadcrumbs";

type Props = {
    isWishlist?: boolean;
    isSearch?: boolean;
    searchProducts?: Products[];
};

type UrlParams = {
    category?: string;
    sub?: string;
};

export const ProductsList = ({
    isWishlist,
    isSearch,
    searchProducts,
}: Props) => {
    const location = useLocation();
    const history = useHistory();

    const searchParams = new URLSearchParams(location.search);
    const { category: urlCategory, sub } = useParams<UrlParams>();

    const [products, setProducts] = useState<Products[]>([]);
    // const [category, setCategory] = useState<CategoriesTypes>();
    const [specCategory, setSpecCategory] = useState<{ name: string }>();
    const [colors, setColors] = useState<ColorTypes[]>([]);
    const [subsName, setSubsName] = useState("");
    const perPage = 18;
    const [isOpen, setIsOpen] = useState(false);
    const [lastCurrentProds, setLastCurrentProds] = useState<Products[]>([]);

    const isSpecial = useMemo(() => {
        if (location.pathname.includes("Specialnoe")) {
            return true;
        }
        return false;
    }, [location.pathname]);

    const categories = useSelector(types.getCategories);
    const specCategs = useSelector(types.getSpecCateg);
    const getColors = useQuery(getColorsQuery);
    const wishList = useSelector(types.getWishList);

    const sortedValue = useMemo(
        () => searchParams.get(sortBy) as SortBy | null,
        [searchParams]
    );
    const currentPage = useMemo(() => searchParams.get("page") || "1", [
        searchParams,
    ]);
    const filterPrice = useMemo(() => searchParams.get("Цена"), [searchParams]);
    const filterColor = useMemo(() => searchParams.get("Цвет"), [searchParams]);
    const filterSizes = useMemo(() => searchParams.get("Размер"), [
        searchParams,
    ]);

    const category = location.pathname.split("/").filter((c) => c)[0];
    const subCateg = location.pathname.split("/").filter((c) => c)[1];

    const currentCategory = categories.find(
        (categ) => handleTranslit(categ.category) === category
    );

    const currentSubCateg = currentCategory?.subCategories.find(
        (subCat) => handleTranslit(subCat.subs) === subCateg
    );

    let searchQuery = "";

    if (location.pathname.includes("/Vse-tovari")) {
        searchQuery = currentCategory?._id || "";
    } else {
        searchQuery = `${currentCategory?._id} / ${currentSubCateg?.id}`;
    }

    const isTablet = useSelector(types.getIsTablet);

    const { data } = useQuery<{
        productsByCategory: Products[];
    }>(productsByCategory, {
        variables: { type: searchQuery },
        skip: isWishlist || isSpecial,
    });

    const { data: specialData, loading: loadingSpecial } = useQuery<{
        productsFromSpecialCategory: SpecialCategory;
    }>(productsFromSpecialCategory, {
        variables: { name: handleDecode(urlCategory || "") },
        skip: !isSpecial,
    });

    useEffect(() => {
        if (
            !loadingSpecial &&
            specialData &&
            specialData.productsFromSpecialCategory
        ) {
            setSpecCategory({
                name: specialData.productsFromSpecialCategory.name,
            });

            const productList = specialData.productsFromSpecialCategory.products.map(
                (item) => ({
                    ...item,
                    sizes: JSON.parse(item.sizes || ""),
                })
            );

            setProducts(productList);
        }
    }, [loadingSpecial, specialData]);

    const openFilters = () => {
        setIsOpen(!isOpen);
        window.scrollTo(0, 0);
    };

    const { data: wishlistData, loading: wishlistLoading } = useQuery<{
        productsFromCart: LocalProduct[];
    }>(getCartProds, {
        variables: { uuid: wishList.map((item) => item.prodId) },
        skip: !isWishlist || wishList.length < 1,
    });

    useEffect(() => {
        if (products.length && !lastCurrentProds.length) {
            setLastCurrentProds(products);
        }

        // eslint-disable-next-line
    }, [products]);

    useEffect(() => {
        if (
            isWishlist &&
            wishlistData &&
            wishlistData.productsFromCart &&
            !wishlistLoading &&
            wishList.length
        ) {
            setProducts(
                wishList.reduce((acc, item) => {
                    const matchedItem = wishlistData.productsFromCart.find(
                        (product) => product.uuid === item.prodId
                    );

                    if (matchedItem) {
                        let sizes: Sizes[] = [];
                        if (typeof matchedItem.sizes === "string") {
                            sizes = JSON.parse(matchedItem.sizes);
                        }

                        acc.push({
                            ...matchedItem,
                            sizes: sizes,
                        });
                    }

                    return acc;
                }, [] as Products[])
            );

            return;
        }

        if (isSearch && searchProducts) {
            setProducts(searchProducts);
            return;
        }

        prodListGoods(
            currentCategory,
            () => {},
            data?.productsByCategory || [],
            setProducts,
            specCategs,
            "",
            setSpecCategory,
            location.pathname,
            setSubsName
        );
    }, [
        currentCategory,
        data,
        isSearch,
        isWishlist,
        location.pathname,
        searchProducts,
        specCategs,
        wishList.length,
        wishlistData,
        wishlistLoading,
        wishList,
    ]);

    useEffect(() => {
        if (getColors.data && getColors.data.colors) {
            setColors(getColors.data.colors);
        }
    }, [getColors]);

    const clearAll = () => {
        history.push({
            search: "",
        });
    };

    const handleCreateTitle = useCallback(() => {
        if (currentCategory && subsName) {
            return `${subsName} - Maritel’`;
        } else if (currentCategory) {
            return `${currentCategory.category} - Maritel’`;
        } else {
            return specCategory?.name ? `${specCategory?.name} - Maritel’` : "";
        }
    }, [specCategory, subsName, currentCategory]);

    useEffect(() => {
        document.title = handleCreateTitle();
        return () => {
            document.title = DEFAULT_PAGE_TITLE;
        };
    }, [handleCreateTitle]);

    useEffect(() => {
        if (isOpen) {
            document.body.classList.add("no-scroll");
        } else {
            document.body.classList.remove("no-scroll");
        }

        return () => {
            document.body.classList.remove("no-scroll");
        };
    }, [isOpen]);

    const handleGetPrices = useMemo(() => prodsSettings.gPrices(products), [
        products,
    ]);

    const sortedProducts = useMemo(
        () => prodsSettings.sortByDropDown(sortedValue || "", products),
        [products, sortedValue]
    );

    const filterByPrice = useMemo(
        () => prodsSettings.fPrice(filterPrice || "", sortedProducts),
        [filterPrice, sortedProducts]
    );

    const handleGetColors = useMemo(
        () => prodsSettings.gColors(filterByPrice, colors),
        [filterByPrice, colors]
    );

    const filterByColor = useMemo(
        () => prodsSettings.fColors(filterColor || "", filterByPrice, colors),
        [filterColor, colors, filterByPrice]
    );

    const handleGetSizes = useMemo(
        () => prodsSettings.gSizes(filterByColor, products),
        [filterByColor, products]
    );

    const filterBySize = useMemo(
        () => prodsSettings.fSizes(filterSizes || "", filterByColor),
        [filterSizes, filterByColor]
    );

    const pagProducts = useMemo(() => {
        setLastCurrentProds(filterBySize);

        return filterBySize.slice(
            perPage * (((currentPage && +currentPage) || 0) - 1),
            perPage * ((currentPage && +currentPage) || 1)
        );
        // eslint-disable-next-line
    }, [filterBySize, currentPage]);

    const makeTitle = () => {
        if (
            +currentPage &&
            +currentPage > 1 &&
            +currentPage < Math.ceil(filterBySize.length / perPage)
        ) {
            return `${perPage * +currentPage}`;
        } else if (
            +currentPage &&
            +currentPage > 1 &&
            +currentPage * perPage > products.length
        ) {
            if (
                (filterColor || filterSizes || filterPrice) &&
                +currentPage === Math.ceil(filterBySize.length / perPage)
            ) {
                return `${filterBySize.length}`;
            }
            return `${products.length}`;
        } else {
            if (+currentPage === Math.ceil(filterBySize.length / perPage)) {
                return `${filterBySize.length}`;
            }
            return `${pagProducts.length}`;
        }
    };

    if (isWishlist) {
        return (
            <div>
                <div className="ProductsList__ProdsWrap">
                    <ul
                        className="ProductsList__Prods"
                        style={{
                            gridTemplateRows: `repeat(${Math.ceil(
                                isTablet && !isOpen
                                    ? pagProducts.length / 2
                                    : isOpen
                                    ? 2
                                    : pagProducts.length / 3
                            )}, minmax(${isTablet ? 210 : 337}px, 1fr))`,
                        }}
                    >
                        {pagProducts.map((prod) => (
                            <ProductCard
                                prod={prod}
                                key={prod.id}
                                products={pagProducts}
                            />
                        ))}
                    </ul>
                </div>
                <Pagination
                    pagesCount={
                        filterColor || filterSizes || filterPrice
                            ? Math.ceil(filterBySize.length / perPage)
                            : Math.ceil(products.length / perPage)
                    }
                    start={!!products.length}
                />
            </div>
        );
    }

    if (isSearch) {
        return (
            <div className="ProductsList Page__Wrap">
                <div className="ProductsList__Wrap">
                    <h1 className="ProductsList__Title">Поиск</h1>
                    <div className="ProductsList__Info">
                        <div
                            className="ProductsList__FilterName ProductsList__FilterName--tablet"
                            onClick={openFilters}
                        >
                            ФИЛЬТР
                        </div>
                        <p className="ProductsList__InfoCount">{`${makeTitle()} из ${
                            filterColor || filterPrice || filterSizes
                                ? filterBySize.length
                                : products.length
                        }`}</p>
                        <div className="ProductsList__SelectWrap">
                            <SelectDropDown
                                values={[
                                    "От новых к старым",
                                    "От старых к новым",
                                    "От дешевых к дорогим",
                                    "От дорогих к дешевым",
                                ]}
                            />
                        </div>
                    </div>
                    <div className="ProductsList__ProductsWrap">
                        <div
                            className={cn({
                                ProductsList__Filters: true,
                                "ProductsList__Filters--open": isOpen,
                            })}
                        >
                            <div className="ProductsList__TabletFilter">
                                <p className="ProductsList__TabletText">
                                    ФИЛЬТР
                                </p>
                                <div
                                    className="ProductsList__TabletX"
                                    onClick={openFilters}
                                />
                            </div>
                            <div className="ProductsList__FilterName">
                                ФИЛЬТР
                            </div>
                            <div className="ProductsList__FilterTabletWrap">
                                <FilterBy
                                    name="Цена"
                                    options={handleGetPrices}
                                />
                                <FilterBy
                                    name="Цвет"
                                    options={handleGetColors}
                                />
                                <FilterBy
                                    name="Размер"
                                    options={handleGetSizes}
                                />
                            </div>
                            <p
                                onClick={clearAll}
                                className="ProductsList__ClearFilters"
                            >
                                Очистить все
                            </p>
                            <div className="ProductsList__ClearTabl">
                                <p
                                    onClick={() => setIsOpen(!isOpen)}
                                    className="ProductsList__ClearTabl--clear ProductsList__ClearTabl--accept"
                                >
                                    Применить
                                </p>
                                <p
                                    onClick={clearAll}
                                    className="ProductsList__ClearTabl--clear"
                                >
                                    Очистить все
                                </p>
                            </div>
                        </div>
                        <div className="ProductsList__ProdsWrap">
                            {pagProducts.length === 0 && (
                                <div
                                    style={{
                                        fontWeight: 800,
                                        fontSize: "14px",
                                        textAlign: "center",
                                    }}
                                >
                                    Ничего не найдено
                                </div>
                            )}

                            <ul
                                className="ProductsList__Prods"
                                style={{
                                    gridTemplateRows: `repeat(${Math.ceil(
                                        isTablet && !isOpen
                                            ? pagProducts.length / 2
                                            : isOpen
                                            ? 2
                                            : pagProducts.length / 3
                                    )}, minmax(${
                                        isTablet ? 210 : 337
                                    }px, 1fr))`,
                                }}
                            >
                                {pagProducts.map((prod) => (
                                    <ProductCard
                                        prod={prod}
                                        key={prod.id}
                                        products={pagProducts}
                                    />
                                ))}
                            </ul>
                        </div>
                    </div>
                </div>
                <Pagination
                    pagesCount={
                        filterColor || filterSizes || filterPrice
                            ? Math.ceil(filterBySize.length / perPage)
                            : Math.ceil(products.length / perPage)
                    }
                    start={!!products.length}
                />
            </div>
        );
    }

    const urlParams = [];
    if (isSpecial) {
        urlParams.push({
            url: `${urlCategory!}/Specialnoe`,
            name: specCategory?.name,
        });
    } else {
        urlParams.push({ url: urlCategory!, name: currentCategory?.category });
        urlParams.push({ url: sub!, name: subsName });
    }

    return products.length > 0 ? (
        <div className="ProductsList Page__Wrap">
            <div className="ProductsList__Wrap">
                <Breadcrumbs path={urlParams} />
                <h1 className="ProductsList__Title">
                    {handleCreateTitle() || "список желаний"}
                </h1>
                <div className="ProductsList__Info">
                    <div
                        className="ProductsList__FilterName ProductsList__FilterName--tablet"
                        onClick={openFilters}
                    >
                        ФИЛЬТР
                    </div>
                    <p className="ProductsList__InfoCount">{`${makeTitle()} из ${
                        filterColor || filterPrice || filterSizes
                            ? filterBySize.length
                            : products.length
                    }`}</p>
                    <div className="ProductsList__SelectWrap">
                        <SelectDropDown
                            values={[
                                "От новых к старым",
                                "От старых к новым",
                                "От дешевых к дорогим",
                                "От дорогих к дешевым",
                            ]}
                        />
                    </div>
                </div>
                <div className="ProductsList__ProductsWrap">
                    <div
                        className={cn({
                            ProductsList__Filters: true,
                            "ProductsList__Filters--open": isOpen,
                        })}
                    >
                        <div className="ProductsList__TabletFilter">
                            <p className="ProductsList__TabletText">ФИЛЬТР</p>
                            <div
                                className="ProductsList__TabletX"
                                onClick={openFilters}
                            />
                        </div>
                        <div className="ProductsList__FilterName">ФИЛЬТР</div>
                        <div className="ProductsList__FilterTabletWrap">
                            <FilterBy name="Цена" options={handleGetPrices} />
                            <FilterBy name="Цвет" options={handleGetColors} />
                            <FilterBy name="Размер" options={handleGetSizes} />
                        </div>
                        <p
                            onClick={clearAll}
                            className="ProductsList__ClearFilters"
                        >
                            Очистить все
                        </p>
                        <div className="ProductsList__ClearTabl">
                            <p
                                onClick={() => setIsOpen(!isOpen)}
                                className="ProductsList__ClearTabl--clear ProductsList__ClearTabl--accept"
                            >
                                Применить
                            </p>
                            <p
                                onClick={clearAll}
                                className="ProductsList__ClearTabl--clear"
                            >
                                Очистить все
                            </p>
                        </div>
                    </div>
                    <div className="ProductsList__ProdsWrap">
                        <ul
                            className="ProductsList__Prods"
                            style={{
                                gridTemplateRows: `repeat(${Math.ceil(
                                    isTablet && !isOpen
                                        ? pagProducts.length / 2
                                        : isOpen
                                        ? 2
                                        : pagProducts.length / 3
                                )}, minmax(${isTablet ? 210 : 337}px, 1fr))`,
                            }}
                        >
                            {pagProducts.map((prod) => (
                                <ProductCard
                                    prod={prod}
                                    key={prod.id}
                                    products={pagProducts}
                                />
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
            {products.length && (
                <Pagination
                    pagesCount={
                        filterColor || filterSizes || filterPrice
                            ? Math.ceil(filterBySize.length / perPage)
                            : Math.ceil(products.length / perPage)
                    }
                    start={!!products.length}
                />
            )}
        </div>
    ) : (
        <div className="Page__Wrap">
            <SpinnerLoader />
        </div>
    );
};
