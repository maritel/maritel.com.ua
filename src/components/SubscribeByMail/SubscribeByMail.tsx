import React, { useEffect, useState } from "react";
import "./SubscribeByMail.scss";
import cn from "classnames";
import { Redirect, useLocation } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

// eslint-disable-next-line
const EMAIL_REGEXP = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;

export const SubscribeByMail = () => {
    const location = useLocation();

    const [isAgreeWithRules, setIsAgreeWithRules] = useState(false);
    const [email, setEmail] = useState("");

    const [errors, setErrors] = useState({ agree: false, email: false });

    const [redirectToRegister, setRedirectToRegister] = useState(false);

    const addSubs = async () => {
        if (EMAIL_REGEXP.test(email.toLocaleLowerCase()) && isAgreeWithRules) {
            setRedirectToRegister(true);
        } else {
            setErrors((prev) => ({ agree: !isAgreeWithRules, email: true }));
        }
    };

    useEffect(() => {
        setRedirectToRegister(false);
    }, [location]);

    return (
        <div className="SubscribeByMail__Invite">
            {redirectToRegister && (
                <Redirect to={`/login?email=${email.toLocaleLowerCase()}`} />
            )}
            <div className="SubscribeByMail__Info">
                <h1 className="SubscribeByMail__Title">
                    ПРИГЛАШАЕМ В MARITEL КЛУБ
                </h1>
                <p className="SubscribeByMail__Discount">
                    Будь в курсе всех новинок и акций
                </p>
            </div>
            <form
                className="SubscribeByMail__Form"
                onSubmit={(e) => e.preventDefault()}
            >
                <div className="SubscribeByMail__FormWrap">
                    <label htmlFor="inputSubs"></label>
                    <input
                        id="inputSubs"
                        type="text"
                        className={cn({
                            SubscribeByMail__InputInvite: true,
                            "SubscribeByMail__InputInvite--error":
                                errors.agree || errors.email,
                        })}
                        placeholder="Ваш e-mail"
                        value={email}
                        onChange={(e) => {
                            setEmail(e.target.value);
                            setErrors({ agree: false, email: false });
                        }}
                    />
                    <button
                        className="SubscribeByMail__ButtonInvite"
                        type="submit"
                        onClick={addSubs}
                        disabled={errors.agree || errors.email}
                    >
                        подписаться
                    </button>
                </div>
                <label className="SubscribeByMail__Privacy">
                    <label
                        className="Maritel__Checkbox__Container"
                        htmlFor="Maritel_AgreeWithRulesCheckbox"
                    >
                        <p
                            className={cn({
                                SubscribeByMail__PrivacyText: true,
                                "SubscribeByMail__PrivacyText--error":
                                    errors.agree,
                            })}
                        >
                            Я соглашаюсь с политикой конфиденциальности
                        </p>
                        <input
                            id="Maritel_AgreeWithRulesCheckbox"
                            className="Maritel__Checkbox"
                            type="checkbox"
                            checked={isAgreeWithRules}
                            onChange={(e) => {
                                setIsAgreeWithRules(e.target.checked);
                                setErrors({ agree: false, email: false });
                            }}
                        />
                        <span
                            className={cn({
                                SubscribeByMail__Checkbox: true,
                                "SubscribeByMail__Checkbox--error":
                                    errors.agree,
                            })}
                        >
                            {isAgreeWithRules && (
                                <FontAwesomeIcon icon={faCheck} />
                            )}
                        </span>
                    </label>
                    {/* //     <input
                    //         type="checkbox"
                    //         className="SubscribeByMail__InviteCheckbox"
                    //         checked={isAgreeWithRules}
                    //         onChange={(e) => {
                    //             setIsAgreeWithRules(e.target.checked);
                    //             setError(false);
                    //         }}
                    //     /> */}
                </label>
            </form>
        </div>
    );
};
