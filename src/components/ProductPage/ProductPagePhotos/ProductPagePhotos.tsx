import React, {
    useCallback,
    useEffect,
    useLayoutEffect,
    useRef,
    useState,
} from "react";
import "./ProductPagePhotos.scss";
import { ProductPageImg } from "../ProductPageImg";
import cn from "classnames";
import { SpinnerLoader } from "../../SpinnerLoader";
import { useSelector } from "react-redux";
import { getQickViewStatus, getIsTablet } from "../../../store/actionsTypes";

interface Rects {
    zoomedContainerRect: DOMRect;
    imageRect: DOMRect;
    zoomedImageRect: DOMRect;
}

const emptyRect: DOMRect = {
    width: 0,
    bottom: 0,
    height: 0,
    left: 0,
    right: 0,
    top: 0,
    x: 0,
    y: 0,
    toJSON: () => undefined,
};

interface Props {
    product: Products;
    generalPhoto: string;
    handleSetGeneralPhoto: (ph: string) => void;
    generalPhotoLoad: boolean;
    setGeneralPhotoLoad: (st: boolean) => void;
    currentPath?: number;
    handleSlider?: (path: number) => void;
}

export const ProductPagePhotos: React.FC<Props> = ({
    product,
    generalPhoto,
    handleSetGeneralPhoto,
    generalPhotoLoad,
    setGeneralPhotoLoad,
    handleSlider,
    currentPath,
}) => {
    const quickStatus = useSelector(getQickViewStatus);
    const isTablet = useSelector(getIsTablet);
    const myRef = useRef<HTMLImageElement>(null);
    const imageContainerRef = useRef<HTMLDivElement>(null);

    const [height, setHeight] = useState(300);

    const zoomedImageContainerRef = useRef<HTMLDivElement>(null);
    const zoomedImageRef = useRef<HTMLImageElement>(null);
    const zoomedSquareRef = useRef<HTMLDivElement>(null);

    const [showZoomed, setShowZoomed] = useState(false);

    const [rects, setRects] = useState<Rects>({
        zoomedContainerRect: emptyRect,
        imageRect: emptyRect,
        zoomedImageRect: emptyRect,
    });

    const [zoomedContentPercentage, setZoomedContentPercentage] = useState(0.5);

    const handleResize = useCallback(() => {
        if (
            zoomedImageContainerRef.current &&
            zoomedImageRef.current &&
            myRef.current &&
            generalPhotoLoad
        ) {
            const zoomedContainerRect = zoomedImageContainerRef.current.getBoundingClientRect();
            const zoomedImageRect = zoomedImageRef.current.getBoundingClientRect();

            const imageRect = myRef.current.getBoundingClientRect();
            setRects({ zoomedContainerRect, zoomedImageRect, imageRect });
            setZoomedContentPercentage(
                zoomedContainerRect.width / zoomedImageRect.width
            );
        }
    }, [generalPhotoLoad]);

    useLayoutEffect(() => {
        handleResize();
        window.addEventListener("resize", handleResize);
        return () => {
            window.removeEventListener("resize", handleResize);
        };
    }, [
        zoomedImageRef,
        zoomedImageContainerRef,
        myRef,
        showZoomed,
        generalPhotoLoad,
        handleResize,
    ]);

    const moveZoomedSquare = useCallback(
        (e: MouseEvent) => {
            if (
                zoomedImageRef.current &&
                zoomedImageContainerRef.current &&
                myRef.current &&
                zoomedSquareRef.current &&
                imageContainerRef.current
            ) {
                if (!showZoomed) {
                    setShowZoomed(true);
                }

                const { clientX, clientY } = e;

                const offsetX =
                    clientX -
                    imageContainerRef.current.getBoundingClientRect().x;
                const offsetY =
                    clientY -
                    imageContainerRef.current.getBoundingClientRect().y;

                const {
                    imageRect,
                    zoomedContainerRect,
                    zoomedImageRect,
                } = rects;

                const additionalOffset =
                    (imageRect.width * zoomedContainerRect.width) /
                    zoomedImageRect.width;

                const newX =
                    offsetX * 3 - additionalOffset >
                    zoomedImageRect.width - zoomedContainerRect.width
                        ? zoomedImageRect.width - zoomedContainerRect.width
                        : offsetX * 3 - additionalOffset;

                const newY =
                    offsetY * 3 - additionalOffset >
                    zoomedImageRect.height - zoomedContainerRect.height
                        ? zoomedImageRect.height - zoomedContainerRect.height
                        : offsetY * 3 - additionalOffset;

                zoomedSquareRef.current.style.transform = `translate(${
                    newX < 0 ? 0 : newX / 3
                }px, ${newY < 0 ? 0 : newY / 3}px)`;

                zoomedImageRef.current.style.transform = `translate(-${
                    newX < 0 ? 0 : newX
                }px, -${newY < 0 ? 0 : newY}px)`;
            }
        },
        [zoomedImageRef, showZoomed, rects]
    );

    const handle = useCallback(
        (e: MouseEvent) => {
            moveZoomedSquare(e);
        },
        [moveZoomedSquare]
    );

    useEffect(() => {
        if (zoomedSquareRef.current) {
            zoomedSquareRef.current.style.width = `${
                zoomedContentPercentage * 100
            }%`;
            zoomedSquareRef.current.style.paddingBottom = `${
                zoomedContentPercentage * 100
            }%`;
        }
    }, [zoomedContentPercentage]);

    useEffect(() => {
        setGeneralPhotoLoad(false);
    }, [currentPath, setGeneralPhotoLoad, generalPhoto]);

    useEffect(() => {
        setHeight(myRef.current?.offsetHeight || 300);
    }, [myRef, setHeight, generalPhoto]);

    useEffect(() => {
        const src = generalPhoto;
        const img = new Image();
        img.addEventListener("load", () => {
            setGeneralPhotoLoad(true);
        });

        img.src = src;

        if (img.complete) {
            setGeneralPhotoLoad(true);
        }
    }, [generalPhoto, setGeneralPhotoLoad]);

    return (
        <div className="ProductPagePhotos__PhotosWrap">
            <div className="ProductPagePhotos__Photos">
                {product?.photos.map((ph, i) => (
                    <ProductPageImg
                        key={ph}
                        ph={ph}
                        generalPhoto={generalPhoto}
                        handleSetGeneralPhoto={handleSetGeneralPhoto}
                        width={74}
                        height={100}
                        handleSlider={handleSlider}
                        i={i}
                    />
                ))}
            </div>
            <div
                className="ProductPagePhotos__GeneralContainer"
                style={{ minHeight: height }}
            >
                {product.photos.length > 1 && isTablet && handleSlider && (
                    <button
                        className="ProductPagePhotos__Slider ProductPagePhotos__SliderBack"
                        onClick={() => handleSlider(-1)}
                    >
                        <img
                            src="images/productPage/arrowSlider.svg"
                            alt="arrow"
                            className="ProductPagePhotos__SliderImg ProductPagePhotos__SliderImgBack"
                        />
                    </button>
                )}
                <div
                    className="ProductPagePhotos__MainPhotoContainer"
                    ref={imageContainerRef}
                    style={{ position: "relative" }}
                    onMouseEnter={() => {
                        if (imageContainerRef.current) {
                            imageContainerRef.current.addEventListener(
                                "mousemove",
                                (e) => {
                                    e.stopImmediatePropagation();
                                    handle(e);
                                }
                            );
                        }
                        setShowZoomed(true);
                    }}
                    onMouseLeave={() => {
                        if (imageContainerRef.current) {
                            imageContainerRef.current.removeEventListener(
                                "mousemove",
                                handle
                            );
                            setShowZoomed(false);
                        }
                    }}
                >
                    <img
                        src={
                            isTablet &&
                            product.photos.length > 1 &&
                            currentPath !== undefined
                                ? product.photos[currentPath]
                                : generalPhoto
                        }
                        alt="general model"
                        className={cn({
                            ProductPagePhotos__GeneralImg: true,
                            "ProductPagePhotos__GeneralImg--loaded": generalPhotoLoad,
                            "ProductPagePhotos__GeneralImg--popup": quickStatus,
                        })}
                        ref={myRef}
                    />
                    <div
                        ref={zoomedSquareRef}
                        className={cn({
                            ProductPagePhotos__ZoomSquare: true,
                            "ProductPagePhotos__ZoomSquare--hidden": !showZoomed,
                        })}
                    />
                </div>

                {product.photos.length > 1 && isTablet && handleSlider && (
                    <button
                        className="ProductPagePhotos__Slider ProductPagePhotos__SliderNext"
                        onClick={() => handleSlider(1)}
                    >
                        <img
                            src="images/productPage/arrowSlider.svg"
                            alt="arrow"
                            className="ProductPagePhotos__SliderImg ProductPagePhotos__SliderImgNext"
                        />
                    </button>
                )}
                {!generalPhotoLoad && <SpinnerLoader />}
                <div
                    ref={zoomedImageContainerRef}
                    className={cn({
                        ProductPagePhotos__ZoomedImageContainer: true,
                        "ProductPagePhotos__ZoomedImageContainer--hidden": !showZoomed,
                    })}
                >
                    <img
                        src={
                            isTablet &&
                            product.photos.length > 1 &&
                            currentPath !== undefined
                                ? product.photos[currentPath]
                                : generalPhoto
                        }
                        alt="general model"
                        className="ProductPagePhotos__ZoomedImage"
                        ref={zoomedImageRef}
                    />
                </div>
            </div>
        </div>
    );
};
