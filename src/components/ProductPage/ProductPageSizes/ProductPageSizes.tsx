import React, { useEffect, useRef } from "react";
import "./ProductPageSizes.scss";
import cn from "classnames";
import { SizesTable } from "../../SizesTable/SizesTable";
import { useDispatch, useSelector } from "react-redux";
import { getAddToCartError } from "../../../store/actionsTypes";
import { setAddToCartError } from "../../../store/actionCreators";

interface Props {
    prodType: string;
    product: Products;
    choosenSize: string;
    setChoosenSize: (size: Sizes) => void;
}

export const ProductPageSizes: React.FC<Props> = ({
    prodType,
    product,
    choosenSize,
    setChoosenSize,
}) => {
    const dispatch = useDispatch();

    const isAddToCartError = useSelector(getAddToCartError);
    const sizeTextRef = useRef<HTMLParagraphElement>(null);

    useEffect(() => {
        if (isAddToCartError && sizeTextRef.current) {
            const y =
                sizeTextRef.current.getBoundingClientRect().top +
                window.scrollY -
                window.innerHeight / 2;
            window.scrollTo(0, y);
        }
    }, [isAddToCartError]);

    if (!product.sizes.length) {
        return <></>;
    }
    return (
        <div className="ProductPageSizes">
            <div className="ProductPageSizes__Title">
                <p ref={sizeTextRef} className="ProductPageSizes__Text">
                    {isAddToCartError ? (
                        <span style={{ color: "#bb2a36" }}>
                            Выберите размер:
                        </span>
                    ) : (
                        "Размер:"
                    )}
                </p>
                <SizesTable>
                    <p className="ProductPageSizes__Sheets">РАЗМЕРНАЯ СЕТКА</p>
                </SizesTable>
            </div>
            <div className="ProductPageSizes__Wrap">
                <ul className="ProductPageSizes__List">
                    {product.sizes.map((size) => (
                        <li
                            key={size.size}
                            className={cn({
                                ProductPageSizes__Size: true,
                                "ProductPageSizes__Size--choosen":
                                    size.size === choosenSize,
                            })}
                            onClick={() => {
                                dispatch(setAddToCartError(false));
                                setChoosenSize(size);
                            }}
                        >
                            {parseInt(size.stock) > 0 && size.size}
                            {parseInt(size.stock) < 1 && (
                                <>
                                    <p>{size.size}</p>
                                    <br />
                                    <p style={{ fontSize: "10px" }}>подписка</p>
                                </>
                            )}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
};
