import React, { useState } from "react";
import "./ProductPageAddCart.scss";
import cn from "classnames";
import { useDispatch } from "react-redux";
import {
    addToCart,
    setAddToCartError,
    setShowAddedToCart,
} from "../../../store/actionCreators";
import Modal from "react-modal";
import { ProductPageMissingProduct } from "../ProductPageMissingProduct/ProductPageMissingProduct";
interface Props {
    checkForSize: boolean;
    choosenSize: string;
    quantity: string;
    prodUuid: string;
    stock: string;
    price: string;
}

Modal.setAppElement("#root");

export const ProductPageAddCart: React.FC<Props> = ({
    checkForSize,
    choosenSize,
    quantity,
    prodUuid,
    stock,
    price,
}) => {
    const dispatch = useDispatch();

    const [isModalOpen, setIsModalOpen] = useState(false);

    if (!checkForSize) {
        return (
            <button
                type="button"
                className={cn({
                    ProductPageAddCart__Button: true,
                })}
                onClick={() => {
                    dispatch(setShowAddedToCart(true));
                    dispatch(addToCart(prodUuid, quantity, ""));
                }}
            >
                добавить в корзину
            </button>
        );
    }

    if (parseInt(stock) < 1) {
        return (
            <>
                <button
                    type="button"
                    className={cn({
                        ProductPageAddCart__Button: true,
                        "ProductPageAddCart__Button--dis": !choosenSize,
                    })}
                    disabled={!choosenSize}
                    onClick={() => {
                        setIsModalOpen(true);
                    }}
                >
                    Сообщить о поступлении
                </button>
                <Modal
                    className="Maritel__Modal"
                    overlayClassName="Maritel__ModalOverlay"
                    isOpen={isModalOpen}
                    onRequestClose={() => setIsModalOpen(false)}
                >
                    <ProductPageMissingProduct
                        price={price}
                        size={choosenSize}
                        product={prodUuid}
                        closeModal={() => setIsModalOpen(false)}
                    />
                </Modal>
            </>
        );
    }

    return (
        <button
            type="button"
            className={cn({
                ProductPageAddCart__Button: true,
                "ProductPageAddCart__Button--dis": !choosenSize,
            })}
            onClick={() => {
                if (!choosenSize) {
                    dispatch(setAddToCartError(false));
                    setTimeout(() => {
                        dispatch(setAddToCartError(true));
                    }, 0);
                } else {
                    dispatch(setShowAddedToCart(true));
                    dispatch(addToCart(prodUuid, quantity, choosenSize));
                }
            }}
        >
            добавить в корзину
        </button>
    );
};
