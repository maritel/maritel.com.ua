import { Slider } from "@material-ui/core";
import React, { useEffect } from "react";
import { Control, Controller } from "react-hook-form";

interface InputRangeProps {
    control: Control;
    register: any;
    name: string;
    options: { value: any; mark: number; name: string }[];
    spacing?: number;
    min?: number;
}

export const InputRange: React.FC<InputRangeProps> = ({
    control,
    register,
    name,
    options,
    spacing = 1,
    min = 0,
}) => {
    useEffect(() => {
        control.setValue(name, min);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{ width: "100%", margin: "1.5% 1.5%" }}>
            <Controller
                defaultValue={min}
                control={control}
                name={name}
                render={({ onChange, value }) => (
                    <>
                        <Slider
                            name={name}
                            step={spacing}
                            onChange={(_ev, newValue) => {
                                onChange(newValue);
                            }}
                            marks={options.map(({ name, mark }) => ({
                                label: name,
                                value: mark,
                            }))}
                            min={min}
                            max={spacing * (options.length + min - 1)}
                        />
                    </>
                )}
            />
        </div>
    );
};
