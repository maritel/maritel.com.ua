import React, { useState } from "react";
import "../AsyncSelect/AsyncSelect.scss";
import "../Input/Input.scss";
import "./MaritelSelect.scss";

import { Controller, FieldError } from "react-hook-form";

import Autocomplete, {
    createFilterOptions,
} from "@material-ui/lab/Autocomplete";

import cn from "classnames";
import selectArrow from "../../../images/selectArrow.svg";

import { getIsTablet } from "../../../store/actionsTypes";
import { useSelector } from "react-redux";

type Props = {
    onSelect?: (selectedValue: OptionType) => void;
    label?: string;
    name: string;
    placeholder?: string;
    disabled?: boolean;
    disabledMessage?: string;
    required?: boolean;
    style?: { [x: string]: string | number };
    control: any;
    error?: FieldError;
    errorMessage?: string;
    options: OptionType[];
    returnValue?: boolean;
    limit?: number;
    special?: boolean;
};

export const Select = (props: Props) => {
    const {
        label,
        name,
        placeholder,
        disabled,
        disabledMessage,
        required,
        style,
        control,
        error,
        errorMessage,
        options,
        returnValue,
        limit,
    } = props;

    const filterOptions = createFilterOptions<OptionType>({
        limit: limit || 6,
    });

    const isTablet = useSelector(getIsTablet);

    const [open, setOpen] = useState(false);
    const [inputData, setInputData] = useState({ value: "", input: "" });

    const findMatch = (value: string | number | OptionType) => {
        if (typeof value === "string" || typeof value === "number") {
            const match = options.find(
                (item) => item.value.toString() === value.toString()
            );

            if (match) {
                return match.name;
            }
        }

        if (typeof value === "object") {
            const match = options.find((item) =>
                value.value
                    ? item.value.toString() === value.value.toString()
                    : false
            );

            if (match) {
                return match.name;
            }
        }

        return (
            <span className="MaritelSelect__Placeholder">
                {placeholder || `  `}
            </span>
        );
    };

    return (
        <Controller
            control={control}
            name={name}
            defaultValue=""
            render={({ onChange, onBlur, name, value }) =>
                isTablet ? (
                    <div className="Maritel__InputContainer">
                        {label && (
                            <label>
                                {label}{" "}
                                {required && (
                                    <span className="Maritel__Required">*</span>
                                )}
                            </label>
                        )}
                        <div className="Maritel__SelectContainer">
                            <select
                                placeholder={placeholder}
                                className="Maritel__Input"
                                style={{
                                    opacity: 0,
                                    position: "absolute",
                                    zIndex: 20,
                                }}
                                name={name}
                                onChange={({ target }) => {
                                    const match = options.find(
                                        (item) =>
                                            item.value.toString() ===
                                            target.value.toString()
                                    );

                                    if (match) {
                                        if (returnValue) {
                                            onChange(match.value);
                                        } else {
                                            onChange(match);
                                        }
                                    }
                                }}
                            >
                                {(!value || !value?.value) && (
                                    <option
                                        style={{ display: "none" }}
                                    ></option>
                                )}
                                {options.map((option) => (
                                    <option
                                        key={option.value.toString()}
                                        value={option.value}
                                        selected={
                                            value?.value
                                                ? option.value.toString() ===
                                                  value.value.toString()
                                                : false
                                        }
                                    >
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                            <div
                                className={`Maritel__Input ${
                                    error && "Maritel__InputError"
                                }`}
                            >
                                {findMatch(value)}
                            </div>
                            <div
                                className={cn({
                                    Maritel__SelectArrow: true,
                                    "Maritel__SelectArrow--open": open,
                                })}
                            >
                                <img src={selectArrow} alt="select arrow" />
                            </div>
                        </div>
                        {error && (
                            <p className="Maritel__Error">{errorMessage}</p>
                        )}
                    </div>
                ) : (
                    <Autocomplete
                        onBlur={onBlur}
                        onChange={({ target }) => {
                            const index = (target as HTMLLIElement).getAttribute(
                                "data-option-index"
                            ) as string;
                            if (returnValue) {
                                onChange(
                                    options[parseInt(index) as number].value
                                );
                            } else {
                                onChange(options[parseInt(index) as number]);
                            }
                        }}
                        value={
                            returnValue
                                ? options.find(
                                      (option) =>
                                          option.value === value.toString()
                                  )
                                    ? options.find(
                                          (option) =>
                                              option.value === value.toString()
                                      )
                                    : null
                                : value
                                ? value
                                : null
                        }
                        style={style}
                        open={open}
                        onOpen={() => {
                            setOpen(true);
                        }}
                        onClose={() => {
                            setOpen(false);
                        }}
                        getOptionSelected={(option, value) => {
                            return option?.value === value?.value;
                        }}
                        getOptionLabel={(option) =>
                            option?.name ? option.name : ""
                        }
                        onInput={({ target }) => {
                            setInputData({
                                ...inputData,
                                input: (target as HTMLInputElement).value,
                            });
                        }}
                        filterOptions={filterOptions}
                        options={options}
                        noOptionsText={"Ничего не найдено."}
                        className="Maritel__InputContainer"
                        disabled={disabled}
                        data-tip={disabled ? disabledMessage : ""}
                        renderInput={(params) => (
                            <div ref={params.InputProps.ref}>
                                {label && (
                                    <label>
                                        {label}{" "}
                                        {required && (
                                            <span className="Maritel__Required">
                                                *
                                            </span>
                                        )}
                                    </label>
                                )}
                                <div className="Maritel__SelectContainer">
                                    <input
                                        type="text"
                                        placeholder={placeholder}
                                        {...params.inputProps}
                                        className={`Maritel__Input ${
                                            error && "Maritel__InputError"
                                        }`}
                                        name={name}
                                    />
                                    <div
                                        onClick={() => setOpen(!open)}
                                        className={cn({
                                            Maritel__SelectArrow: true,
                                            "Maritel__SelectArrow--open": open,
                                        })}
                                    >
                                        <img
                                            src={selectArrow}
                                            alt="select arrow"
                                        />
                                    </div>
                                </div>
                                {error && (
                                    <p className="Maritel__Error">
                                        {errorMessage}
                                    </p>
                                )}
                            </div>
                        )}
                    />
                )
            }
        />
    );
};
