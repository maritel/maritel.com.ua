import React from "react";
import "./Checkbox.scss";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { Control, Controller } from "react-hook-form";

type Props = {
    control: Control<any>;
    label?: string;
    name: string;
    onChange?: (value: boolean) => void;
};

export const Checkbox: React.FC<Props> = (props) => {
    const { name, label, control } = props;

    return (
        <Controller
            control={control}
            name={name}
            render={({ onChange, onBlur, name, value, ref }) => (
                <label className="Maritel__Checkbox__Container">
                    {label}
                    <input
                        className="Maritel__Checkbox"
                        type="checkbox"
                        name={name}
                        onChange={(e) =>
                            props.onChange
                                ? props.onChange(e.target.checked)
                                : onChange(e.target.checked)
                        }
                        onBlur={onBlur}
                        checked={value}
                        ref={ref}
                    />
                    <span className="Maritel__Checkbox__Checkmark">
                        <FontAwesomeIcon icon={faCheck} />
                    </span>
                </label>
            )}
        />
    );
};
