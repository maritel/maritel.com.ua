import React, { useMemo } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import "./Captcha.scss";

import ReactModal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import {
    setCaptchaPopupState,
    setCaptchaShouldRefetch,
    setCaptchaValue,
} from "../../../store/actionCreators";
import { getCaptchaPopupState } from "../../../store/actionsTypes";

export const Captcha: React.FC = () => {
    const dispatch = useDispatch();

    const isModalOpen = useSelector(getCaptchaPopupState);

    const recaptchaElement = useMemo(() => {
        return (
            <ReCAPTCHA
                sitekey={process.env.REACT_APP_CAPTCHA_PUBLIC || ""}
                onChange={(value) => {
                    setTimeout(() => {
                        dispatch(setCaptchaPopupState(false));
                        dispatch(setCaptchaShouldRefetch(true));
                    }, 0);

                    dispatch(setCaptchaValue(value || ""));
                }}
            />
        );
    }, [dispatch]);

    return (
        <ReactModal
            className="Captcha__Modal"
            overlayClassName="Captcha__ModalOverlay"
            isOpen={isModalOpen}
            onRequestClose={() => dispatch(setCaptchaPopupState(false))}
        >
            <div className="Captcha">
                <h1>Подтвердите, что вы не робот</h1>
                <div className="Captcha__Text">
                    С вашего адреса было сделано слишком много запросов, мы
                    хотим убедиться, что вы человек
                </div>

                {recaptchaElement}
            </div>
        </ReactModal>
    );
};
