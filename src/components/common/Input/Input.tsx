import React from "react";
import "./Input.scss";

import { FieldError } from "react-hook-form";
import selectArrow from "../../../images/selectArrow.svg";

type Props = {
    register: any;
    name: string;
    label?: string;
    error?: FieldError;
    errorMessage?: string;
    className?: string;
    required?: boolean;
    type?: string;
    placeholder?: string;
    autoComplete?: string;
    showArrow?: boolean;
};

export const Input = (props: Props) => {
    const {
        register,
        error,
        name,
        label,
        errorMessage,
        className,
        required,
        type,
        placeholder,
        autoComplete,
        showArrow,
    } = props;

    return (
        <div className={"Maritel__InputContainer " + className}>
            <label>
                {label}{" "}
                <span className="Maritel__Required">{required && "*"}</span>
            </label>
            <div style={{ position: "relative" }}>
                <input
                    placeholder={placeholder}
                    className={error ? "Maritel__InputError" : ""}
                    name={name}
                    ref={register}
                    type={type}
                    autoComplete={autoComplete}
                />
                {showArrow && (
                    <div className={"Maritel__SelectArrow"}>
                        <img src={selectArrow} alt="select arrow" />
                    </div>
                )}
            </div>
            {error && <p className="Maritel__Error">{errorMessage}</p>}
        </div>
    );
};
