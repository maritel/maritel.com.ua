import cn from "classnames";
import React, { useState } from "react";
import "./BackgroundMainImg.scss";

import { useSelector } from "react-redux";
import { getHomePageBanner } from "../../store/actionsTypes";

export const BackgroundMainImg = () => {
    const [showImg, setShowImg] = useState(false);

    const images = useSelector(getHomePageBanner);

    return (
        <>
            <div className="BackgroundMainImg__ImgWrap BackgroundMainImg__Desktop">
                <img
                    src={images.desktop}
                    alt="main"
                    className={cn({
                        BackgroundMainImg__Img: true,
                        "BackgroundMainImg__Img--loaded": showImg,
                    })}
                    onLoad={() => setShowImg(true)}
                />
                {!showImg && <div className="BackgroundMainImg__Preload" />}
            </div>
            <div className="BackgroundMainImg__ImgWrap  BackgroundMainImg__Mobile">
                <img
                    src={images.mobile}
                    alt="main"
                    className={cn({
                        BackgroundMainImg__Img: true,
                        "BackgroundMainImg__Img--loaded": showImg,
                    })}
                    onLoad={() => setShowImg(true)}
                />
                {!showImg && <div className="BackgroundMainImg__Preload" />}
            </div>
        </>
    );
};
