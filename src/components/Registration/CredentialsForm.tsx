import React, { useCallback, useEffect } from "react";
import "./CredentialsForm.scss";

import { useForm } from "react-hook-form";

import { Input } from "../common/Input/Input";

import { useDispatch, useSelector } from "react-redux";
import { Checkbox } from "../common/Checkbox";
import { registerCustomer } from "./api";
import { setCustomerInfo } from "../../store/actionCreators";
import { useLocation } from "react-router-dom";
import { useHandleFetch } from "../../helpers/useHandleFetch";
import { getCaptchaValue } from "../../store/actionsTypes";

type FormType = {
    email: string;
    password: string;
    confirmPassword: string;
    subscribe: boolean;
};

const CredentialsForm = () => {
    const dispatch = useDispatch();

    const location = useLocation();

    const defaultEmail = new URLSearchParams(location.search).get("email");

    const {
        register,
        getValues,
        setValue,
        setError,
        handleSubmit,
        control,
        errors,
    } = useForm<FormType>({
        defaultValues: { email: defaultEmail || undefined },
    });

    const captcha = useSelector(getCaptchaValue);

    const registerCallback = useCallback(() => {
        return registerCustomer(
            getValues().email,
            getValues().password,
            getValues().subscribe,
            captcha
        );
    }, [getValues, captcha]);

    const [{ loading, data, haveResult }, setSend] = useHandleFetch(
        registerCallback
    );

    useEffect(() => {
        if (!loading && !data.ok && haveResult) {
            setError("email", { message: data.error });
        }

        if (!loading && data.ok) {
            dispatch(setCustomerInfo({ accessToken: data.accessToken }));
        }
    }, [loading, data, setError, dispatch, haveResult]);

    const onSubmit = async (data: FormType) => {
        if (data.password !== data.confirmPassword) {
            setError("confirmPassword", {});
        } else {
            setSend();
        }
    };

    return (
        <div className="LoginForm">
            <form onSubmit={handleSubmit(onSubmit)}>
                <Input
                    name="email"
                    label="E-MAIL"
                    register={register({ required: true })}
                    error={errors.email}
                    errorMessage={errors.email?.message}
                />
                <div className="CredentialsForm__PasswordsContainer">
                    <Input
                        type={"password"}
                        name="password"
                        label="ПАРОЛЬ"
                        register={register({ required: true, minLength: 7 })}
                        error={errors.password}
                        autoComplete="new-password"
                    />
                    <Input
                        type={"password"}
                        name="confirmPassword"
                        label="ПОВТОРИТЕ ПАРОЛЬ"
                        register={register({ required: true, minLength: 7 })}
                        error={errors.confirmPassword}
                        autoComplete="new-password"
                    />
                </div>
                <div className="CredentialsForm__Subscribe">
                    <Checkbox label="" name="subscribe" control={control} />
                    <span
                        onClick={() =>
                            setValue("subscribe", !getValues().subscribe)
                        }
                    >
                        Я хочу стать членом Maritel’ клуба и получать обновления
                        последних продуктов и акций по электронной почте и
                        другим каналам.
                    </span>
                </div>
                <button className="LoginForm__Submit" type="submit">
                    РЕГИСТРАЦИЯ
                </button>
            </form>
        </div>
    );
};

export default CredentialsForm;
