export const registerCustomer = (
    email: string,
    password: string,
    subscribe: boolean,
    captcha: string
): Promise<
    { ok: boolean; accessToken?: string; error?: string } | CaptchaError
> => {
    return fetch(`${process.env.REACT_APP_SERVER}/register`, {
        method: "POST",
        credentials: "include",
        mode: "cors",
        body: JSON.stringify({
            email,
            password,
            subscribe,
            "g-recaptcha": captcha,
        }),
        headers: { "Content-type": "application/json" },
    }).then((res) => res.json());
};
