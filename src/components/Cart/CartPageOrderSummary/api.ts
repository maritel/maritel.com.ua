import { RootState } from "../../../store";

export const fetchOrderInfo = (
    order: RootState["order"],
    cart: CartProd[],
    captcha: string,
    accessToken: string,
    promoName: string,
    useBonuses: boolean
) => {
    return fetch(`${process.env.REACT_APP_SERVER}/order`, {
        method: "POST",
        mode: "cors",
        credentials: "include",
        body: JSON.stringify({
            ...order,
            items: cart,
            promoName,
            useBonuses,
            "g-recaptcha": captcha,
        }),
        headers: {
            "Content-type": "application/json",
            authorization: `bearer ${accessToken}`,
        },
    })
        .then((res) => res.json())
        .then((data) => {
            return data;
        });
};

export const addOrderToAmocrm = async (
    orderInfo: RootState["order"],
    cart: CartProd[],
    orderTotal: number,
    products: Products[]
) => {
    try {
        const req = await fetch(
            `${process.env.REACT_APP_SERVER}/amocrm/amocrm-login`
        );

        const res = await req.json();
        if (res.response.auth) {
            const orderProductsInfo = cart.map((item) => {
                const currentProd = products.find(
                    (product) => product.uuid === item.prodUuid
                );
                return {
                    ...item,
                    name: currentProd?.title,
                    price: currentProd?.price,
                };
            });

            const body = JSON.stringify({
                coockie: res.coockie,
                orderInfo,
                orderProductsInfo,
                orderTotal,
            });
            await fetch(`${process.env.REACT_APP_SERVER}/amocrm/create-lead`, {
                method: "POST",
                body,
            });
        }
    } catch (err) {
        console.log(err);
    }
};
