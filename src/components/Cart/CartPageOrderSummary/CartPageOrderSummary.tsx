import React, {
    ReactElement,
    useCallback,
    useEffect,
    useRef,
    useState,
} from "react";
import "./CartPageOrderSummary.scss";

import { useDispatch, useSelector } from "react-redux";
import {
    getAccessToken,
    getCaptchaValue,
    getCart,
    getCartItemsTotal,
    getOrderCommision,
    getOrderTotal,
    getProducts,
    getPromo,
    getUseBonuses,
    getUsedBonuses,
} from "../../../store/actionsTypes";
import { orderSchema } from "../../../helpers/validationSchemas";
import { fetchOrderInfo, addOrderToAmocrm } from "./api";
import { setOrderStatus } from "../../../store/actionCreators";
import { DeepMap, FieldError, UseFormMethods } from "react-hook-form";
import { CartFormType } from "../../../pages/CartPage/CartPage";
import { Promo } from "../../Promo";
import { Redirect } from "react-router-dom";
import { useHandleFetch } from "../../../helpers/useHandleFetch";
import { CartUseBonuses } from "../CartUseBonuses/CartUseBonuses";

import cn from "classnames";

type Props = {
    formMethods: UseFormMethods<CartFormType>;
};

interface CreateOrderError {
    status: 400;
    err: string;
}

interface CreateOrderSuccess {
    status?: undefined;
    action?: string;
    orderId: string;
    amount?: number;
    method: "GET" | "POST";
    fields: {
        [x: string]: string | number | number[] | string[];
    };
}

type CreateOrderResponse = CreateOrderError | CreateOrderSuccess;

export const CartPageOrderSummary = ({ formMethods }: Props) => {
    const dispatch = useDispatch();

    const cartItemsTotal = useSelector(getCartItemsTotal);
    const [orderInfo, setOrderInfo] = useState({} as CartFormType);
    const orderTotal = useSelector(getOrderTotal);
    const cart = useSelector(getCart);
    const accessToken = useSelector(getAccessToken);
    const promo = useSelector(getPromo);
    const products = useSelector(getProducts);

    const useBonuses = useSelector(getUseBonuses);
    const bonusCount = useSelector(getUsedBonuses);

    const orderCommision = useSelector(getOrderCommision);
    const captcha = useSelector(getCaptchaValue);

    const [active, setActive] = useState(false);

    const formRef = useRef<HTMLFormElement>(null);

    const [orderData, setOrderData] = useState(
        null as null | { [x: string]: any }
    );

    const [orderSuccess, setOrderSuccess] = useState(false);

    useEffect(() => {
        setActive(!orderSchema.validate(formMethods.getValues()).error);
    }, [formMethods]);

    const orderDataInputs: ReactElement[] = [];

    const addOrderCallback = useCallback(() => {
        return fetchOrderInfo(
            formMethods.getValues(),
            cart,
            captcha,
            accessToken || "",
            promo.promoName,
            useBonuses
        );
    }, [accessToken, captcha, cart, formMethods, promo.promoName, useBonuses]);

    const [
        { data, loading, haveResult },
        setSend,
    ] = useHandleFetch<CreateOrderResponse>(addOrderCallback);

    useEffect(() => {
        if (active && !loading && haveResult) {
            if (data.status === 400) {
                return;
            }

            setOrderData(data);
            setTimeout(() => {
                localStorage.setItem(
                    "orderStatus",
                    JSON.stringify({
                        orderId: orderData?.orderId,
                        status:
                            orderInfo.paymentMethod === "cash"
                                ? "accepted"
                                : "pending",
                    })
                );

                dispatch(
                    setOrderStatus({
                        orderId: orderData?.orderId,
                        status:
                            orderInfo.paymentMethod === "cash"
                                ? "accepted"
                                : "pending",
                    })
                );

                if (
                    process.env.NODE_ENV === "production" &&
                    !process.env.HEROKU
                ) {
                    addOrderToAmocrm(orderInfo, cart, orderTotal, products);
                }

                if (orderInfo.paymentMethod === "cash") {
                    setOrderSuccess(true);
                }

                if (data && data.action && formRef.current) {
                    if (data.method === "GET") {
                        window.location.href = data.action;
                        return;
                    }
                    formRef.current.submit();
                }
            }, 0);
        }
    }, [
        active,
        cart,
        data,
        dispatch,
        haveResult,
        loading,
        orderData,
        orderInfo,
        orderTotal,
        products,
    ]);

    if (orderData) {
        const { fields } = orderData;
        for (const key in fields) {
            if (Array.isArray(fields[key])) {
                for (const el of fields[key]) {
                    orderDataInputs.push(
                        <input
                            type="hidden"
                            key={`${key}${el}`}
                            name={`${key}[]`}
                            value={el}
                        />
                    );
                }
            } else {
                orderDataInputs.push(
                    <input
                        type="hidden"
                        key={key}
                        name={key}
                        value={fields[key]}
                    />
                );
            }
        }
    }

    const submitOrder = (data: CartFormType) => {
        if (active) {
            setOrderInfo(data);
            setSend();
        } else {
            formMethods.trigger().then(() => {
                if (formMethods.errors) {
                    const errorRef = getErrorRef<CartFormType>(
                        formMethods.errors
                    );

                    if (errorRef) {
                        errorRef.focus();
                    }
                }
            });
        }
    };

    if (orderSuccess) {
        return (
            <Redirect
                to={`/order-success?amount=${
                    (data as CreateOrderSuccess).amount || ""
                } `}
            />
        );
    }

    return (
        <div className="CartPageOrderSummary">
            <Promo />
            <div className="CartPageOrderSummary__Title">ВАШ ЗАКАЗ</div>
            <div className="CartPageOrderSummary__VertLine" />
            <div className="CartPageOrderSummary__Item">
                <span className="CartPageOrderSummary__ItemsTotal">
                    ТОВАРОВ НА СУММУ
                </span>
                <span>{cartItemsTotal} грн.</span>
            </div>
            <div className="CartPageOrderSummary__VertLine" />
            {/* {formMethods.getValues().shippingMethod && (
                <>
                    <div className="CartPageOrderSummary__Item">
                        <span>ДОСТАВКА</span>
                        <span>{shippingCost}</span>
                    </div>
                    <div className="CartPageOrderSummary__VertLine" />
                </>
            )} */}
            {promo.promoValue > 0 && (
                <>
                    <div className="CartPageOrderSummary__Item">
                        <span>ПРОМО-КОД</span>
                        <span>
                            {promo.promoValue}{" "}
                            {promo.promoDisc === "grn" ? "грн." : "%"}
                        </span>
                    </div>
                    <div className="CartPageOrderSummary__VertLine" />
                </>
            )}
            {/* {formMethods.getValues().paymentMethod === "card" && (
                <>
                    <div className="CartPageOrderSummary__Item">
                        <span>КОМИССИЯ (2,5%)</span>
                        <span>{orderCommision} грн.</span>
                    </div>
                    <div className="CartPageOrderSummary__VertLine" />
                </>
            )} */}
            {useBonuses && (
                <>
                    <div className="CartPageOrderSummary__Item">
                        <span>ОПЛАТА БОНУСАМИ</span>
                        <span className="CartPageOrderSummary__Discount">
                            -{bonusCount} грн.
                        </span>
                    </div>
                    <div className="CartPageOrderSummary__VertLine" />
                </>
            )}
            <div className="CartPageOrderSummary__Title CartPageOrderSummary__Item">
                <span>ИТОГО К ОПЛАТЕ</span>
                <span>{orderTotal} грн.</span>
            </div>
            <CartUseBonuses />
            <div
                className={cn({
                    CartPageOrderSummary__Submit: true,
                    CartPageOrderSummary__Submit__Error: !active,
                })}
                onClick={formMethods.handleSubmit(submitOrder)}
            >
                ПОДТВЕРДИТЬ ЗАКАЗ
            </div>
            <div>
                <form
                    method="post"
                    action={orderData?.action}
                    acceptCharset="utf-8"
                    ref={formRef}
                >
                    {orderDataInputs}
                </form>
            </div>
        </div>
    );
};

const getErrorRef = <T extends unknown>(errors: DeepMap<T, FieldError>) => {
    const getFirstProp = (error: { [x: string]: any }) => {
        for (const prop in error) {
            return error[prop];
        }
    };

    let error = getFirstProp(errors);

    while (error && !error.hasOwnProperty("ref")) {
        try {
            error = getFirstProp(error);
        } catch {
            return null;
        }
    }

    if (error) {
        return error.ref as HTMLInputElement;
    } else {
        return null;
    }
};
