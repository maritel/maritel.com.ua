import cn from "classnames";
import React, { useEffect, useMemo, useState } from "react";
import { useQuery } from "react-apollo";
import { useDispatch } from "react-redux";
import { getCartProds, getColorsQuery } from "../../../helpers";
import { delFromCart } from "../../../store/actionCreators";
import { CartProdItemSelect } from "../../CartProdItemSelect";
import { SpinnerLoader } from "../../SpinnerLoader";
import "./CartProdItem.scss";

interface Props {
    uuid: string;
    size: string;
    quantity: string;
}

export const CartProdItem: React.FC<Props> = ({ uuid, size, quantity }) => {
    const dispatch = useDispatch();
    const getColors = useQuery(getColorsQuery);
    const [product, setProduct] = useState<Products>();
    const [photoLoaded, setPhotoLoaded] = useState(false);
    const [colors, setColros] = useState<ColorTypes[]>([]);
    const { data: prodData } = useQuery<{ productsFromCart: Products[] }>(
        getCartProds,
        { variables: { uuid: [uuid] } }
    );

    useEffect(() => {
        if (prodData && prodData.productsFromCart) {
            const { productsFromCart } = prodData;
            const prod = productsFromCart[0];

            if (typeof prod.sizes === "string") {
                const parsedSizes = JSON.parse((prod.sizes as never) as string);
                prod.sizes = parsedSizes;
            }
            setProduct(prod);
        }
    }, [prodData]);

    useEffect(() => {
        if (getColors && getColors.data && getColors.data.colors) {
            setColros(getColors.data.colors);
        }
    }, [getColors]);

    const unitsInStock = useMemo(() => {
        if (product && size) {
            if (product.sizes.length > 0) {
                const matchedSize = product.sizes.find((s) => s.size === size);
                if (matchedSize) {
                    return matchedSize.stock;
                }
                return "1";
            } else {
                return "5";
            }
        }
        return "1";
    }, [product, size]);

    const matchedColor = useMemo(() => {
        if (colors && product) {
            return colors.find((c) => product?.color === c.id)?.name;
        }
        return "";
    }, [colors, product]);

    return (
        <li className="CartProdItem__Item">
            <div className="CartProdItem__Wrap">
                <div className="CartProdItem__LeftSide">
                    <div className="CartProdItem__ImgWrap">
                        <img
                            src={product?.previewPhoto}
                            className={cn({
                                CartProdItem__PrImg: true,
                                "CartProdItem__PrImg--loaded": photoLoaded,
                            })}
                            alt="preview ph"
                            onLoad={() => setPhotoLoaded(true)}
                        />
                        {!photoLoaded && <SpinnerLoader />}
                    </div>
                    <div className="CartProdItem__Inf">
                        <p className="CartProdItem__Title">{product?.title}</p>
                        <p className="CartProdItem__SmallInfo">
                            {matchedColor}
                            {matchedColor && size && (
                                <span className="CartProdItem__SmallInfSpan">
                                    |
                                </span>
                            )}
                            {size}
                            {/* <span className="CartProdItem__Change">
                                изменить
                            </span> */}
                        </p>
                    </div>
                </div>
                <div className="CartProdItem__RightSide">
                    <p className="CartProdItem__Price">{product?.price} грн.</p>
                    {product?.lastPrice && (
                        <p className="CartProdItem__LastPrice">
                            {product.lastPrice} грн.
                        </p>
                    )}
                    {product && (
                        <CartProdItemSelect
                            quantity={quantity}
                            prodUuid={product.uuid}
                            size={size}
                            maxQ={unitsInStock}
                        />
                    )}
                </div>
            </div>

            <p className="CartProdItem__Del">
                <span onClick={() => dispatch(delFromCart(uuid + size))}>
                    Удалить
                </span>
            </p>
        </li>
    );
};
