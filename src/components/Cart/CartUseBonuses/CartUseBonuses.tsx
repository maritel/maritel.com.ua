import React, { useEffect } from "react";
import "./CartUseBonuses.scss";

import { useQuery } from "react-apollo";
import { useDispatch, useSelector } from "react-redux";
import { getCustomer } from "../../../helpers";
import { getIsLogged, getUseBonuses } from "../../../store/actionsTypes";
import { Checkbox } from "../../common/Checkbox";
import { SpinnerLoader } from "../../SpinnerLoader";
import { useForm } from "react-hook-form";
import { setBonuses, setUseBonuses } from "../../../store/actionCreators";

interface CartUseBonusesProps {}

type FormType = {
    useBonuses: boolean;
};

export const CartUseBonuses: React.FC<CartUseBonusesProps> = () => {
    const dispatch = useDispatch();

    const isLogged = useSelector(getIsLogged);

    const useBonuses = useSelector(getUseBonuses);

    const { loading, data } = useQuery<{ customer: Customer }>(getCustomer);

    const { control, reset } = useForm<FormType>({
        defaultValues: { useBonuses: useBonuses },
    });

    useEffect(() => {
        reset({ useBonuses: useBonuses });
    }, [reset, useBonuses]);

    useEffect(() => {
        if (!loading && data && data.customer) {
            dispatch(setBonuses(data.customer.bonuses));
        }
    }, [data, dispatch, loading]);

    if (!isLogged) {
        return <></>;
    }

    if (loading) {
        return <SpinnerLoader />;
    }

    if (!data || (data && !data.customer)) {
        return <></>;
    }

    return (
        <div className="CartUseBonuses">
            <div className="CartUseBonuses__Amount">
                БОНУСНЫЙ СЧЕТ: <span>{data.customer.bonuses} грн.</span>
            </div>
            <div className="CartUseBonuses__Checkbox">
                <Checkbox
                    control={control}
                    name="useBonuses"
                    label="Оплатить заказ бонусами"
                    onChange={(value: boolean) => {
                        dispatch(setUseBonuses(value));
                    }}
                />
            </div>
        </div>
    );
};
