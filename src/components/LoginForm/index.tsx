import React, { useCallback, useEffect } from "react";
import "./LoginForm.scss";

import { useForm } from "react-hook-form";

import { Input } from "../common/Input/Input";

import { login } from "./api";
import { Checkbox } from "../common/Checkbox";
import { useDispatch, useSelector } from "react-redux";
import { setCustomerInfo } from "../../store/actionCreators";
import { useHandleFetch } from "../../helpers/useHandleFetch";
import { getCaptchaValue } from "../../store/actionsTypes";

type FormType = {
    email: string;
    password: string;
    remember: boolean;
};

const LoginForm = () => {
    const dispatch = useDispatch();

    const captcha = useSelector(getCaptchaValue);

    const {
        register,
        setError,
        handleSubmit,
        getValues,
        control,
        errors,
    } = useForm<FormType>({});

    const loginCallback = useCallback(() => {
        return login(
            getValues().email,
            getValues().password,
            getValues().remember,
            captcha
        );
    }, [getValues, captcha]);

    const [{ loading, data, haveResult }, setSend] = useHandleFetch(
        loginCallback
    );

    useEffect(() => {
        if (!loading && !data.ok && haveResult) {
            setError("email", {});
            setError("password", {});
        }

        if (!loading && data.ok) {
            dispatch(setCustomerInfo({ accessToken: data.accessToken }));
        }
    }, [loading, data, setError, dispatch, haveResult]);

    const onSubmit = async (data: FormType) => {
        setSend();
    };

    return (
        <div className="LoginForm">
            <form onSubmit={handleSubmit(onSubmit)}>
                <Input
                    name="email"
                    label="E-MAIL"
                    register={register({ required: true })}
                    error={errors.email}
                />
                <Input
                    type={"password"}
                    name="password"
                    label="ПАРОЛЬ"
                    register={register({ required: true, minLength: 7 })}
                    error={errors.password}
                />
                <div className="LoginForm__Additional">
                    <Checkbox
                        label="Запомнить"
                        name="remember"
                        control={control}
                    />
                    {/* <span>Забыли пароль?</span> */}
                </div>
                <button className="LoginForm__Submit" type="submit">
                    ВОЙТИ
                </button>
            </form>
        </div>
    );
};

export default LoginForm;
