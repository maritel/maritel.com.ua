interface LoginResponse {
    ok: boolean;
    accessToken: string;
}

export const login = (
    email: string,
    password: string,
    remember: boolean,
    captcha: string
): Promise<LoginResponse | CaptchaError> => {
    return fetch(`${process.env.REACT_APP_SERVER}/login`, {
        method: "POST",
        credentials: "include",
        mode: "cors",
        body: JSON.stringify({
            email,
            password,
            remember,
            "g-recaptcha": captcha,
        }),
        headers: { "Content-type": "application/json" },
    })
        .then((res) => res.json())
        .then((data) => {
            return data;
        });
};
