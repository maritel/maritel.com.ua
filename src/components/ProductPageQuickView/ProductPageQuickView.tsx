import React, { useEffect, useState } from "react";
import { useQuery } from "react-apollo";
import { useDispatch, useSelector } from "react-redux";
import { getColorsQuery, productQuery, splitValue, productsByTitle } from "../../helpers";
import {
    getCategories,
    getQickViewUuid,
    getCart,
} from "../../store/actionsTypes";
import * as Prod from "../ProductPage";
import { SpinnerLoader } from "../SpinnerLoader";
import "./ProductPageQuickView.scss";
import {
    setQucikViewStatus,
    setQucikViewUuid,
} from "../../store/actionCreators";
import ReactBreakLines from "../../helpers/ReactBreakLines";

export const ProductPageQuickView = () => {
    const dispatch = useDispatch();
    const uuid = useSelector(getQickViewUuid);
    const getColors = useQuery(getColorsQuery);
    const categories = useSelector(getCategories);
    const { data: prod } = useQuery<{ product: Products }>(productQuery, {variables: { uuid }})

    const [product, setProduct] = useState<Products>();
    const [relatedProds, setRelatedProds] = useState<Products[]>([]);
    const [colors, setColors] = useState<ColorTypes[]>([]);
    const [generalPhoto, setGeneralPhoto] = useState("");
    const [generalPhotoLoad, setGeneralPhotoLoad] = useState(false);
    const [choosenSize, setChoosenSize] = useState<Sizes>({} as Sizes);
    const [quantity, setQuantity] = useState("1");
    const cart = useSelector(getCart);
    const { data: relProds } = useQuery<{productsByName: Products[]}>(productsByTitle, {variables: { title: product?.title }})

    useEffect(() => {
        if (relProds && relProds.productsByName && product) {

            setRelatedProds(
                [...relProds.productsByName.map(prod => ({
                    ...prod,
                    sizes: JSON.parse(prod.sizes as never as string)
                }))
                .filter(
                    (prod) =>
                        prod.title === product.title &&
                        prod.id !== product.id &&
                        prod.color !== product.color
                ), product]
            )
        }
    },[relProds, product])

    useEffect(() => {
        if (prod && prod.product) {
            const { product } = prod
            const sizes = JSON.parse(product.sizes as never as string)
            product.sizes = sizes
            setProduct(product)
            setGeneralPhoto(product.previewPhoto || "");
            setChoosenSize({} as Sizes);
            setQuantity("1");
        }
    }, [prod])

    useEffect(() => {
        if (getColors && getColors.data) {
            setColors(getColors.data.colors);
        }
    }, [getColors, setColors]);

    const handleSetGeneralPhoto = (ph: string) => {
        setGeneralPhoto(ph);
    };

    useEffect(() => {
        setQuantity("1");
        setChoosenSize({} as Sizes);
    }, [cart]);

    return product && colors.length ? (
        <div className="ProductPageQuickView">
            <label className="ProductPageQuickView__CloseLabel">
                <button
                    className="ProductPageQuickView__Close"
                    type="button"
                    onClick={() => {
                        dispatch(setQucikViewStatus(false));
                        dispatch(setQucikViewUuid(""));
                    }}
                >
                    <img
                        src="/images/productPage/closeX2x.png"
                        alt="close"
                        className="ProductPageQuickView__Img"
                    />
                </button>
            </label>
            <Prod.ProductPagePhotos
                product={product}
                generalPhoto={generalPhoto}
                handleSetGeneralPhoto={handleSetGeneralPhoto}
                generalPhotoLoad={generalPhotoLoad}
                setGeneralPhotoLoad={setGeneralPhotoLoad}
            />
            <div className="ProductPageQuickView__Info">
                <Prod.ProductPageProdInfo product={product} />
                <div className="ProductPageQuickView__ProdGeneralInfo">
                    <Prod.ProductPageColor colors={colors} product={product} />
                    <Prod.ProductPageRelated
                        colors={colors}
                        relatedProds={relatedProds}
                        product={product}
                        generalPathName={""}
                        popup={true}
                    />
                    <div className="ProductPageQuickView__SizesWrap">
                        <Prod.ProductPageSizes
                            prodType={
                                categories.find(
                                    (categ) =>
                                        categ._id ===
                                        product.type.split(splitValue)[0]
                                )?.category!
                            }
                            product={product}
                            choosenSize={choosenSize.size}
                            setChoosenSize={setChoosenSize}
                        />
                        <Prod.ProductPageQuantity
                            quantity={quantity}
                            setQuantity={setQuantity}
                            choosenSize={choosenSize.size}
                            product={product}
                        />
                        <Prod.ProductPageAddCart
                            price={product.price}
                            checkForSize={!!product.sizes.length}
                            choosenSize={choosenSize.size}
                            prodUuid={product.uuid}
                            quantity={quantity}
                            stock={choosenSize.stock}
                        />
                    </div>
                    <div className="ProductPageQuickView__Descr">
                        <p className="ProductPageQuickView__DescrText">
                            <ReactBreakLines text={product.descr} />
                        </p>
                    </div>
                    <Prod.ProductPageInfoCard
                        title="Параметры модели"
                        text={product.modelParam}
                    />
                    <Prod.ProductPageInfoCard
                        title="Уход за изделием"
                        text={product.care}
                    />
                    <Prod.ProductPageInfoCard
                        title="Состав"
                        text={product.composition}
                    />
                    <Prod.ProductShareProd
                        name={product.title}
                        img={product.previewPhoto}
                    />
                </div>
            </div>
        </div>
    ) : (
        <div className="ProductPageQuickView">
            <SpinnerLoader />
        </div>
    );
};
