import React, { useEffect, useMemo } from "react";
import "./CategoryPage.scss";
import { useLocation, Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { getCategories } from "../../store/actionsTypes";
import translit from "cyrillic-to-translit-js";
import { DEFAULT_PAGE_TITLE } from "../../helpers";
import { handleTranslit } from "../../helpers/links";
import { Banner } from "../../components/common/Banner";

export const CategoryPage = () => {
    const location = useLocation();
    const categories = useSelector(getCategories);

    const categNameFromUrl = location.pathname
    .split("/")
    .filter((name) => name)[0];

    const currentCategory = categories.find(
        (categ) =>
            new translit().transform(
                categ.category.toLocaleLowerCase(),
                "+"
            ) === categNameFromUrl
    );

    const [bannerTop, bannerBottom] = useMemo(() => {
        if (
            currentCategory &&
            currentCategory.banners.length &&
            currentCategory.banners[0] &&
            (currentCategory.banners[0] as BannerCategory).position
        ) {
            const result: JSX.Element[] = [];
            const topBanner = (currentCategory.banners as BannerCategory[]).find(
                ({ position }) => position === "top"
            );

            if (topBanner) {
                result.push(
                    <Banner
                        text={topBanner.title}
                        buttonText={topBanner.buttonText}
                        imgLink={topBanner.image}
                        link={topBanner.link}
                    />
                );
            }

            const bottomBanner = (currentCategory.banners as BannerCategory[]).find(
                ({ position }) => position === "bottom"
            );

            if (bottomBanner) {
                result.push(
                    <Banner
                        text={bottomBanner.title}
                        buttonText={bottomBanner.buttonText}
                        imgLink={bottomBanner.image}
                        link={bottomBanner.link}
                    />
                );
            }

            return result;
        }

        return [null, null];
    }, [currentCategory]);

    useEffect(() => {
        if (currentCategory) {
            document.title = `${currentCategory.category} - Maritel’`;
        }
        return () => {
            document.title = DEFAULT_PAGE_TITLE;
        };
    }, [currentCategory]);

    return (
        <div className="CategoryPage Page__Wrap">
            {bannerTop}
            <div className="CategoryPage__Wrap">
                <h2 className="CategoryPage__Title">категории</h2>
                <ul className="CategoryPage__List">
                    {currentCategory?.subCategories.map((prod) => {
                    return (
                        <li className="CategoryPage__Item" key={prod.id}>
                            <Link
                                to={`/${handleTranslit(
                                    currentCategory?.category || ""
                                )}/${handleTranslit(prod.subs || ""
                                )}`}
                            >
                                {/* <img
                                    src=''
                                    alt="preview ph"
                                    className="CategoryPage__Img"
                                /> */}
                                <p className="CategoryPage__Name">
                                    {
                                        prod.subs
                                    }
                                </p>
                            </Link>
                        </li>
                    )})}
                </ul>
            </div>
            {bannerBottom}
        </div>
    );
};
