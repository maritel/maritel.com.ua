import React, { useEffect } from "react";
import "./BonusesPage.scss";

import { AccountMenu } from "../../components/AccountMenu";
import { useQuery } from "react-apollo";
import { DEFAULT_PAGE_TITLE, getCustomer } from "../../helpers";
import { SpinnerLoader } from "../../components/SpinnerLoader";
import { getIsLogged } from "../../store/actionsTypes";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";

export const BonusesPage: React.FC = () => {
    const { data, loading } = useQuery<{ customer: Customer }>(getCustomer);

    const isLogged = useSelector(getIsLogged);

    useEffect(() => {
        document.title = "Бонусный счет - Maritel’";
        return () => {
            document.title = DEFAULT_PAGE_TITLE;
        };
    }, []);

    if (loading && isLogged) {
        return (
            <div className="Page__Wrap">
                <SpinnerLoader />
            </div>
        );
    }

    if (
        (!loading && !data) ||
        (!loading && data && data.customer && !data.customer._id)
    ) {
        return <Redirect to="/login" />;
    }

    return (
        <div className="WishlistPage Page__Wrap">
            <AccountMenu selected="bonuses" disabled={!isLogged} />
            <div className="AccountPage__RightContent">
                <div className="Account__Greetings">
                    Добро пожаловать, {data?.customer.firstName}
                </div>

                <div className="Account__PageTitle">БОНУСНЫЙ СЧЕТ</div>

                <div className="BonusesPage__Container">
                    <div>
                        <p className="BonusesPage__Accumulated">Накоплено:</p>
                        <p>
                            <span className="BonusesPage__Amount">
                                {data?.customer.bonuses}
                            </span>{" "}
                            <span className="BonusesPage__Currency">грн</span>
                        </p>
                    </div>
                    <div className="BonusesPage__Info">
                        Накапливайте бонусные гривны и используйте их при оплате
                        своих покупок.
                    </div>
                </div>
            </div>
        </div>
    );
};
