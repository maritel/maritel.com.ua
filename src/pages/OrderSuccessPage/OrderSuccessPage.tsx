import React, { useEffect, useRef, useState } from "react";
import "./OrderSuccessPage.scss";
import { useDispatch } from "react-redux";
import { clearCart } from "../../store/actionCreators";
import CheckmarkIcon from "../../images/checkmark.svg";
import { Redirect, useLocation } from "react-router-dom";

declare global {
    interface Window {
        fbq: any;
    }
}

const OrderSuccessPage = () => {
    const dispatch = useDispatch();

    const location = useLocation();

    const searchParams = new URLSearchParams(location.search);

    const timerRef = useRef<ReturnType<typeof setTimeout>>();

    const [redirect, setRedirect] = useState(false);

    useEffect(() => {
        timerRef.current = setTimeout(() => {
            setRedirect(true);
        }, 7000);

        return () => {
            const { current } = timerRef;
            if (current) {
                clearTimeout(current);
            }
        };
    }, []);

    const requestSentRef = useRef(false);

    useEffect(() => {
        const amount = parseFloat(searchParams.get("amount") || "");

        if (amount && !requestSentRef.current) {
            requestSentRef.current = true;
            if (process.env.NODE_ENV === "production" && !process.env.HEROKU) {
                if (window.fbq) {
                    window.fbq("track", "Purchase", {
                        value: parseFloat(
                            parseFloat((amount / 3).toString()).toFixed(2)
                        ),
                        currency: "UAH",
                    });
                }
            }

            dispatch(clearCart());
        }
    }, [dispatch, searchParams]);

    if (redirect) {
        return <Redirect to="/" />;
    }

    return (
        <div className="OrderSuccess Page__Wrap">
            <div className="OrderSuccess__Wrap">
                <div>
                    <div className="OrderSuccess__SuccessCheckmark">
                        <img src={CheckmarkIcon} alt="" />
                    </div>
                    <p className="OrderSuccess__Text">
                        ВЫ УСПЕШНО ОФОРМИЛИ ЗАКАЗ.
                        <br />
                        ОЖИДАЙТЕ ЗВОНКА ДЛЯ ПОДТВЕРЖДЕНИЯ ДЕТАЛЕЙ
                    </p>
                </div>
            </div>
        </div>
    );
};

export default OrderSuccessPage;
