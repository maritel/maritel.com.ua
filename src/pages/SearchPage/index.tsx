import React, { useEffect, useState } from "react";
import { useQuery } from "react-apollo";
import { useLocation } from "react-router-dom";
import { ProductsList } from "../../components/ProductsList";
import { DEFAULT_PAGE_TITLE, findProducts } from "../../helpers";

export const SearchPage = () => {
    const { search } = useLocation();

    const [products, setProducts] = useState<Products[]>([])


    const searchParams = new URLSearchParams(search);

    const query = searchParams.get("query") || "";

    const { data: items } = useQuery<{
        queryProducts: Products[]
    }>(findProducts, {variables: { query }})

    useEffect(() => {
        if (query && items && items.queryProducts) {
            const { queryProducts: foundProducts } = items

            setProducts(foundProducts.map(prod => ({
                ...prod,
                sizes: JSON.parse(prod.sizes as never as string)
            })))
        } else {
            setProducts([])
        }
    }, [items, query])


    useEffect(() => {
        document.title = "Поисковой запрос - Maritel’";
        return () => {
            document.title = DEFAULT_PAGE_TITLE;
        };
    }, []);

    return (
        <div className="Page__Wrap">
            <ProductsList isSearch={true} searchProducts={products} />
        </div>
    );
};
