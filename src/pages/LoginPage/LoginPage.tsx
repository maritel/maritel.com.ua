import React, { useEffect } from "react";
import "./LoginPage.scss";
import { useQuery } from "react-apollo";
import { useSelector } from "react-redux";
import { getCustomer } from "../../helpers/gqlQuery";
import LoginForm from "../../components/LoginForm";
import RegistrationForm from "../../components/Registration/CredentialsForm";
import { getIsLogged } from "../../store/actionsTypes";
import { Redirect } from "react-router-dom";
import ReactBreakLines from "../../helpers/ReactBreakLines";
import { SpinnerLoader } from "../../components/SpinnerLoader";
import { GiftIcon } from "../../images/gift";
import { StarIcon } from "../../images/star";
import { CakeIcon } from "../../images/cake";
import { MuseumIcon } from "../../images/museum";
import { DEFAULT_PAGE_TITLE } from "../../helpers";

const LoginPage = () => {
    const isLogged = useSelector(getIsLogged);

    const { data, loading } = useQuery<{ customer: Customer }>(getCustomer, {
        skip: !isLogged,
    });

    useEffect(() => {
        document.title = "Войти - Maritel’";
        return () => {
            document.title = DEFAULT_PAGE_TITLE;
        };
    }, []);

    if (loading) {
        return (
            <div className="Page__Wrap">
                <SpinnerLoader />
            </div>
        );
    }

    if (data && data.customer.status === "registering") {
        return <Redirect to="/register" />;
    }

    if (data && data.customer.status === "registered") {
        return <Redirect to="/account" />;
    }

    if (isLogged) {
        return <Redirect to="/account" />;
    }

    return (
        <div className="LoginPage Page__Wrap">
            <div className="LoginPage__LoginFormContainer">
                <div className="LoginPage__Title">ПОСТОЯННЫЙ КЛИЕНТ</div>
                <LoginForm />
            </div>
            <div className="LoginPage__RegistrationFormContainer">
                <div className="LoginPage__Title">
                    <ReactBreakLines
                        text={
                            "ПРИСОЕДИНИТЬСЯ СЕЙЧАС\n И НАСЛАЖДАТЬСЯ ПРЕИМУЩЕСТВАМИ"
                        }
                    />
                </div>
                <RegistrationForm />
            </div>
            <div className="LoginPage__IconListContainer">
                <div>
                    <GiftIcon />
                    <br />
                    ПРИВЕТСТВЕННЫЙ
                    <br />
                    БОНУС
                </div>
                <div>
                    <StarIcon />
                    <br />
                    ЭКСКЛЮЗИВНЫЕ
                    <br />
                    ПРЕДЛОЖЕНИЯ
                </div>
                <div>
                    <CakeIcon />
                    <br />
                    БОНУСЫ
                    <br />
                    КО ДНЮ РОЖДЕНИЯ
                </div>
                <div>
                    <MuseumIcon />
                    <br />
                    VIP
                    <br />
                    ДОСТУП
                </div>
            </div>
        </div>
    );
};

export default LoginPage;
