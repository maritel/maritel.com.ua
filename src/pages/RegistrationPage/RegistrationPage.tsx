import React, { useEffect, useState } from "react";
import "./RegistrationPage.scss";
import { useMutation, useQuery } from "react-apollo";
import { useSelector } from "react-redux";
import { getCustomer } from "../../helpers/gqlQuery";
import { useForm } from "react-hook-form";
import { getIsLogged } from "../../store/actionsTypes";
import { Redirect } from "react-router-dom";
import { Input } from "../../components/common/Input/Input";
import AsyncSelect from "../../components/common/AsyncSelect/AsyncSelect";
import {
    getCityWarehouses,
    getDeliveryAddress,
    getNVSettlements,
} from "./../../helpers/novaposhtaAPI";
import { Select } from "../../components/common/Select/Select";
import { DEFAULT_PAGE_TITLE, editCustomer } from "../../helpers";
import { SpinnerLoader } from "../../components/SpinnerLoader";
import { validatePhone } from "../../helpers/validate";

const months = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
];

type FormType = {
    firstName: string;
    lastName: string;
    city: OptionType;
    shippingMethod?: { value: "postOffice" | "courier"; name: string };
    shippingAddress: {
        street?: OptionType;
        houseNumber?: string;
        appartment?: string;
        value?: string;
        name?: string;
    };
    birthday?: {
        day: string;
        month: string;
    };
    phone: string;
    gender: { value: "male" | "female"; name: string };
};

export const RegistrationPage = () => {
    const {
        register,
        watch,
        setValue,
        getValues,
        setError,
        reset,
        handleSubmit,
        trigger,
        errors,
        control,
    } = useForm<FormType>();

    const isLogged = useSelector(getIsLogged);

    const { data, loading } = useQuery<{ customer: Customer }>(getCustomer, {
        skip: !isLogged,
    });

    const [redirect, setRedirect] = useState("");

    const [updateCustomer] = useMutation(editCustomer);

    useEffect(() => {
        document.title = "Персональные данные - Maritel’";
        return () => {
            document.title = DEFAULT_PAGE_TITLE;
        };
    }, []);

    const [days, setDays] = useState<OptionType[]>([]);

    useEffect(() => {
        const day = parseInt(getValues().birthday?.day!);
        let daysInAMonth = 31;

        if (getValues().birthday?.month) {
            const month = parseInt(getValues().birthday?.month!);
            const date = new Date(2020, month + 1, 0);
            daysInAMonth = date.getDate();
        }

        if (day > daysInAMonth) {
            setValue("birthday.day", daysInAMonth.toString());
        } else if (day < 1) {
            setValue("birthday.day", "1");
        }

        const result: OptionType[] = [];

        for (let i = 1; i <= daysInAMonth; i++) {
            result.push({ name: `${i}`, value: `${i}` });
        }

        setDays(result);

        const selectedDay = getValues().birthday?.day;

        if (selectedDay && parseInt(selectedDay) > daysInAMonth) {
            reset({ birthday: { day: daysInAMonth.toString() } });
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [getValues().birthday?.month, getValues().birthday?.day]);

    useEffect(() => {
        if (!loading && data?.customer) {
            const { customer } = data;
            const defaultFormValues: { [x: string]: any } = {};

            setValue("shippingMethod", defaultFormValues.shippingMethod);
            reset(customer);
            trigger();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loading]);

    if (loading) {
        return (
            <div className="Page__Wrap">
                <SpinnerLoader />
            </div>
        );
    }

    if (!isLogged && !data?.customer) {
        return <Redirect to="/login" />;
    }

    if (redirect) {
        return <Redirect to={redirect} />;
    }

    watch("shippingMethod");
    watch("shippingMethod.name");
    watch("shippingMethod.value");
    watch("city");
    watch("birthday");

    const { shippingMethod, city } = getValues();

    const onSubmit = (fieldData: FormType) => {
        if (fieldData) {
            if (fieldData.birthday?.day || fieldData.birthday?.month) {
                if (!fieldData.birthday?.day) {
                    setError("birthday.day", {});
                    return;
                }
                if (!fieldData.birthday?.month) {
                    setError("birthday.month", {});
                    return;
                }
            }

            updateCustomer({
                variables: {
                    _id: "5f66116ca7f08c3f98193af3",
                    customer: {
                        ...{
                            ...fieldData,
                            shippingAddress: {
                                street: {},
                                houseNumber: "",
                                appartment: "",
                                value: "",
                                name: "",
                                ...fieldData.shippingAddress,
                            },
                        },
                        status: "registered",
                    },
                },
                update: (store, { data }) => {
                    if (!data) {
                        return null;
                    }

                    store.writeQuery({
                        query: getCustomer,
                        data: { customer: data?.EditCustomerInfo },
                    });
                    setRedirect("/account");
                },
            });
        }
    };

    return (
        <div className="RegistrationPage Page__Wrap">
            <div className="RegistrationPage__Title">информация о вас</div>
            <div className="RegistrationPage__Container">
                <div className="RegistrationPage__PersonalInformation">
                    {data?.customer.status === "registering" && (
                        <div>
                            Спасибо за регистрацию. Заполните свой профиль и
                            делайте покупки с легкостью.
                        </div>
                    )}
                    <div>
                        <span className="Maritel__Required">* </span>
                        Обязательные поля
                    </div>
                    <div>
                        <span>Email: </span> {data?.customer.email}
                    </div>
                    <div>
                        <span>Пароль:</span> {"  "}*******
                    </div>
                    <div>
                        <span>Maritel' Club Member ID:</span>{" "}
                        {data?.customer._id}
                    </div>
                </div>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <Input
                        required={true}
                        name="firstName"
                        label="Имя"
                        register={register({ required: true, minLength: 3 })}
                        error={errors.firstName}
                    />
                    <Input
                        required={true}
                        name="lastName"
                        label="Фамилия"
                        register={register({ required: true, minLength: 3 })}
                        error={errors.lastName}
                    />
                    <AsyncSelect
                        placeholder="Выберите город"
                        label="Город"
                        name="city"
                        control={control}
                        getOptions={getNVSettlements}
                    />
                    <Input
                        required={true}
                        name="phone"
                        label="Мобильный номер"
                        register={register({
                            required: true,
                            validate: validatePhone,
                        })}
                        error={errors.phone}
                    />
                    <Select
                        label="Доставка"
                        name="shippingMethod"
                        control={control}
                        options={[
                            { value: "postOffice", name: "В отделение" },
                            { value: "courier", name: "Курьером" },
                        ]}
                    />
                    {shippingMethod?.value === "postOffice" && (
                        <AsyncSelect
                            placeholder="Выберите отделение"
                            label="Отделение Новой Почты"
                            name="shippingAddress"
                            control={control}
                            getOptions={(searchQuery: string) =>
                                getCityWarehouses(searchQuery, city?.value)
                            }
                        />
                    )}
                    {shippingMethod?.value === "courier" && (
                        <>
                            <div className="CartPage__CourierDelivery">
                                <AsyncSelect
                                    name="shippingAddress.street"
                                    control={control}
                                    style={{
                                        flexBasis: "56%",
                                    }}
                                    label="Улица"
                                    getOptions={(searchQuery: string) =>
                                        getDeliveryAddress(
                                            searchQuery,
                                            getValues().city?.value
                                        )
                                    }
                                    error={
                                        errors.shippingAddress?.street?.value
                                    }
                                    errorMessage="Укажите улицу"
                                    required={true}
                                />
                                <Input
                                    label="Дом"
                                    name="shippingAddress.houseNumber"
                                    register={register({
                                        required: true,
                                    })}
                                    className="Maritel__InputContainer__Small"
                                    error={errors.shippingAddress?.houseNumber}
                                    errorMessage="Укажите дом"
                                    required={true}
                                />
                                <Input
                                    label="Кв./Оф."
                                    name="shippingAddress.appartment"
                                    register={register({})}
                                    className="Maritel__InputContainer__Small"
                                    error={errors.shippingAddress?.appartment}
                                    errorMessage="Укажите квартиру"
                                />
                            </div>
                        </>
                    )}
                    <Select
                        placeholder="Выберите свой пол"
                        label="Пол"
                        name="gender"
                        control={control}
                        options={[
                            { value: "male", name: "Мужской" },
                            { value: "female", name: "Женский" },
                        ]}
                    />
                    <div className="RegistrationPage__BirthdayContainer">
                        <div className="RegistrationPage__BirthdayTitle">
                            Ваш день рождения (мы любим дарить подарки)
                        </div>
                        <Select
                            placeholder="День"
                            control={control}
                            name="birthday.day"
                            options={days}
                            error={errors.birthday?.day}
                            limit={days.length}
                            returnValue={true}
                        />
                        <Select
                            placeholder="Месяц"
                            name="birthday.month"
                            control={control}
                            options={months.map((month, index) => ({
                                name: month,
                                value: index.toString(),
                            }))}
                            error={errors.birthday?.month}
                            returnValue={true}
                            limit={12}
                        />
                    </div>
                    <div>
                        <button className="LoginForm__Submit" type="submit">
                            ПОДТВЕРДИТЬ
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
};
