import React, { useCallback, useEffect, useRef, useState } from "react";
import { useQuery } from "react-apollo";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch, useLocation } from "react-router-dom";
import { Footer } from "./components/common/Footer";
import { Header } from "./components/common/Header";
import { ProductsList } from "./components/ProductsList";
import * as helpers from "./helpers";
import { HomePage } from "./pages";
import { CategoryPage } from "./pages/CategoryPage";
import * as aCreator from "./store/actionCreators";
import {
    getMenuStatus,
    getQickViewStatus,
    getIsTablet,
    getBackgroundSearchCover,
    getWishList,
    getCart,
} from "./store/actionsTypes";
import "./styles/index.scss";
import { ProductPage } from "./pages/ProductPage";
import { ProductPageQuickView } from "./components/ProductPageQuickView";
import { CartPage } from "./pages/CartPage";
import OrderSuccessPage from "./pages/OrderSuccessPage/OrderSuccessPage";
import LoginPage from "./pages/LoginPage/LoginPage";
import { AccountPage } from "./pages/AccountPage/AccountPage";
import { RegistrationPage } from "./pages/RegistrationPage/RegistrationPage";
import { WishlistPage } from "./pages/WishlistPage";
import { OrderHistoryPage } from "./pages/OrderHistoryPage";
import { SearchPage } from "./pages/SearchPage";
import { AboutUsPage } from "./staticPages/AboutUsPage/AboutUsPage";
import { Captcha } from "./components/common/Captcha/Captcha";
import { BonusesPage } from "./pages/BonusesPage/BonusesPage";
import { getCartProds } from "./helpers";

import DesktopImg from "./images/desktop.png";
import MobileImg from "./images/mobile.png";

function App() {
    const location = useLocation();
    const dispatch = useDispatch();
    const backgroundStatus = useSelector(getMenuStatus);
    const [scrollPos, setScrollPos] = useState(0);
    const [cartProdsItems, setCartProdsItems] = useState<Products[]>([]);
    const [headerVisible, setHeaderVisible] = useState(true);
    const quickViewSt = useSelector(getQickViewStatus);
    const isTablet = useSelector(getIsTablet);
    const searchBackground = useSelector(getBackgroundSearchCover);
    const wishList = useSelector(getWishList);
    const cart = useSelector(getCart);

    const { data, loading } = useQuery<{
        // products: LocalProduct[];
        homePageBanner: BannerHomePage;
        specialBanner: BannerSpecial;
        categories: CategoriesFromDB[];
        getSpecCateg: SpecProdsCategory[];
        mainSettings: MainSettings;
        carousel: BannerCarousel[];
    }>(helpers.initialQuery);

    const cartFilteredRef = useRef(false);

    const { data: cartProds } = useQuery<{ productsFromCart: Products[] }>(
        getCartProds,
        {
            variables: { uuid: cart.map((el) => el.prodUuid) },
        }
    );

    useEffect(() => {
        if (!loading && data && data.homePageBanner) {
            const { homePageBanner } = data;
            const homePageBannerData: BannerHomePage = {
                desktop: homePageBanner.desktop || DesktopImg,
                mobile: homePageBanner.mobile || MobileImg,
            };

            dispatch(aCreator.setHomePageBanner(homePageBannerData));
        }
    }, [loading, data, dispatch]);

    useEffect(() => {
        if (cartProds && cartProds.productsFromCart) {
            setCartProdsItems(cartProds.productsFromCart);
            if (!cartFilteredRef.current) {
                cartFilteredRef.current = true;
                const filteredCart: CartProd[] = [];
                for (const cartItem of cart) {
                    const match = cartProds.productsFromCart.find(
                        (item) => item.uuid === cartItem.prodUuid
                    );
                    if (match) {
                        filteredCart.push(cartItem);
                    }
                }
                dispatch(aCreator.setCartItems(filteredCart));
            }
        }
    }, [cartProds, cart, dispatch]);

    useEffect(() => {
        dispatch(aCreator.setProducts(cartProdsItems));
    }, [cart, dispatch, cartProdsItems]);

    useEffect(() => {
        const searchParams = new URLSearchParams(location.search);
        const query = searchParams.get("query");
        if (query) {
            aCreator.setSearchQuery(query);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleScroll = useCallback(() => {
        const scrollInfo = document.documentElement.getBoundingClientRect();
        setScrollPos(scrollInfo.top);

        if (scrollInfo.top < scrollPos && scrollInfo.top <= -100) {
            setHeaderVisible(false);
        } else {
            setHeaderVisible(true);
        }
    }, [scrollPos]);

    useEffect(() => {
        return () => {
            document.removeEventListener("scroll", handleScroll);
        };
    }, [handleScroll]);

    useEffect(() => {
        localStorage.setItem("wishList", JSON.stringify(wishList));
    }, [wishList]);

    useEffect(() => {
        if (!loading) {
            document.getElementById("full-page-loader")!.style.display = "none";
        }
    }, [loading]);

    useEffect(() => {
        if (!loading) {
            document.addEventListener("scroll", handleScroll);
        }
    }, [scrollPos, handleScroll, loading]);

    useEffect(() => {
        if (data && data.categories) {
            const categories: CategoriesTypes[] = data.categories.map(
                (category) => ({
                    ...category,
                    subCategories: JSON.parse(category.subCategories),
                })
            );

            dispatch(
                aCreator.setCategories(
                    categories.sort((a, b) =>
                        a.category.localeCompare(b.category)
                    )
                )
            );
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data, dispatch]);

    useEffect(() => {
        if (data?.getSpecCateg) {
            dispatch(
                aCreator.setSpecCategories(
                    data.getSpecCateg.filter((categ) => !!categ.products.length)
                )
            );
        }
        if (data?.mainSettings) {
            dispatch(aCreator.setMainSettings(data.mainSettings));
        }

        if (data?.carousel) {
            dispatch(
                aCreator.setCarousel(data.carousel.map((item) => item.title))
            );
        }

        if (data?.specialBanner) {
            dispatch(aCreator.setSpecialBanner(data?.specialBanner));
        }
    }, [data, dispatch]);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [location]);

    useEffect(() => {
        dispatch(aCreator.setQucikViewStatus(false));
        dispatch(aCreator.setQucikViewUuid(""));
    }, [isTablet, dispatch]);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cart));
    }, [cart]);

    return (
        <>
            <Header visible={headerVisible} />
            <Captcha />
            <Switch>
                <Route
                    path="/:category/:sub/:name/:id"
                    exact
                    component={ProductPage}
                />
                <Route
                    path="/:category/Vse-tovari"
                    exact
                    component={ProductsList}
                />
                <Route
                    path="/:category/Specialnoe"
                    exact
                    component={ProductsList}
                />
                <Route path="/about/:path" exact component={AboutUsPage} />
                <Route path="/:category/:sub" exact component={ProductsList} />
                <Route path="/wish-list" exact component={ProductsList} />
                <Route path="/cart" exact component={CartPage} />
                <Route
                    path="/order-success"
                    exact
                    component={OrderSuccessPage}
                />
                <Route path="/orders" exact component={OrderHistoryPage} />
                <Route path="/account" exact component={AccountPage} />
                <Route path="/wishlist" exact component={WishlistPage} />
                <Route path="/bonuses" exact component={BonusesPage} />
                <Route path="/login" exact component={LoginPage} />
                <Route path="/register" exact component={RegistrationPage} />
                <Route path="/search" exact component={SearchPage} />
                <Route path="/:category" exact component={CategoryPage} />
                <Route path="/" exact component={HomePage} />
            </Switch>
            <Footer />
            {backgroundStatus && isTablet && (
                <div
                    className="cover"
                    onClick={() => dispatch(aCreator.setMenuStatus(false))}
                />
            )}
            {searchBackground && (
                <div
                    className="search__cover"
                    onClick={() =>
                        dispatch(aCreator.setBackgroundStatus(false))
                    }
                />
            )}
            {quickViewSt && !isTablet && (
                <>
                    <div
                        className="full__cover"
                        onClick={() => {
                            dispatch(aCreator.setQucikViewStatus(false));
                            dispatch(aCreator.setQucikViewUuid(""));
                        }}
                    />
                    <ProductPageQuickView />
                </>
            )}
        </>
    );
}

export default App;
