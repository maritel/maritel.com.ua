/// <reference types="react-scripts" />

interface SubCateg {
    id: string;
    subs: string;
}

type SortBy =
    | "От новых к старым"
    | "От старых к новым"
    | "От дешевых к дорогим"
    | "От дорогих к дешевым";

interface CategoriesTypes {
    _id: string;
    category: string;
    subCategories: SubCateg[];
    banners: BannerCategory[] | BannerSpecial[];
}

interface CategoriesFromDB {
    _id: string;
    category: string;
    subCategories: string;
    banners: BannerCategory[] | BannerSpecial[];
}

interface SpecProdsCategory {
    id: string;
    name: string;
    products: string[];
}

interface SpecialCategory {
    id: string;
    name: string;
    products: LocalProduct[];
}

interface SubFooterInfo {
    name: string;
    link: string;
}

interface FooterInfo {
    name: string;
    fields: SubFooterInfo[];
}

interface Sizes {
    size: string;
    articul: string;
    stock: string;
}

interface Products {
    uuid: string;
    id: string;
    title: string;
    descr: string;
    color: string | null;
    price: string;
    modelParam: string;
    gender: string;
    care: string;
    composition: string;
    sizes: Sizes[];
    lastPrice: string;
    type: string;
    photos: string[];
    previewPhoto: string;
    timestamp: string;
}

interface LocalProduct {
    id: string;
    uuid: string;
    title: string;
    descr: string;
    color: string | null;
    price: string;
    modelParam: string;
    gender: string;
    care: string;
    composition: string;
    sizes: string | null;
    lastPrice: string;
    type: string;
    photos: string[];
    previewPhoto: string;
    timestamp: string;
}

interface ColorTypes {
    id: string;
    name: string;
    link: string;
}

interface SortOptions {
    name: string;
    count: number;
}

interface CartProd {
    prodUuid: string;
    quantity: string;
    size: string;
}

interface Customer {
    _id: string;
    email: string;
    firstName: string;
    lastName: string;
    city: OptionType;
    phone: string;
    shippingMethod?: { value: "postOffice" | "courier"; name: string };
    shippingAddress: {
        street?: OptionType;
        houseNumber?: string;
        appartment?: string;
        value?: string;
        name?: string;
    };
    birthday: {
        day: string;
        month: string;
    };
    orders: Order[];
    gender: { value: "male" | "female"; name: string };
    status: "registering" | "registered";
    bonuses: number;
}

interface OptionType {
    value: string;
    name: string;
}

interface Order {
    _id: string;
    uuid: string;
    orderId: string;
    items: [
        {
            prodUuid: string;
            name: string;
            size: string;
            quantity: string;
            price: string;
        }
    ];
    date: string;
    receiver?: {
        firstName: string;
        lastName: string;
        patronymic: string;
        phone: string;
    };
    payer: {
        firstName: string;
        lastName: string;
        phone: string;
    };
    city: OptionType;
    customReceiver: Boolean;
    paymentMethod: string;
    paymentService: string;
    shippingAddress: {
        street?: OptionType;
        appartment?: string;
        houseNumber?: string;
        value?: string;
        name?: string;
    };
    shippingMethod: string;
    amount: string;
    paymentStatus: string;
    deliveryStatus: string;
}

interface MainSettings {
    main?: string;
    phone?: string;
    instagram?: string;
    facebook?: string;
    telegram?: string;
}

interface SocialLink {
    name: "facebook" | "instagram" | "telegram";
    link: string;
}

type Promo = {
    promoName: string;
    promoDisc: "grn" | "percent";
    promoValue: number;
};

type InfoPageNames =
    | "brand"
    | "bloggers"
    | "vacancies"
    | "delivery-policies"
    | "return-policies"
    | "rules"
    | "public-offer";

type BannerCategory = {
    position: "top" | "bottom";
    title?: string;
    buttonText?: string;
    link?: string;
    image: string;
};

type BannerSpecial = {
    accentedText: string;
    text: string;
    buttonText: string;
    link: string;
};

type BannerCarousel = {
    title: string;
};

interface BannerHomePage {
    desktop?: string;
    mobile?: string;
}

interface CaptchaError {
    error: "captcha";
}
